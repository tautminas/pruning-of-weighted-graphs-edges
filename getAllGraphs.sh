#!/bin/bash

: '''
Usage example:
bash getAllGraphs.sh protein_interactions_with_areas_no_domains

Output:
Directory "Graphs_%Y.%m.%d_%H:%M:%S\" is filled with graphs.
Fraction of completed work is written to stdout.
'''

COUNTER=0

DIR_NAME=Graphs_$(date +%Y.%m.%d_%H:%M:%S)
mkdir "$DIR_NAME"

PDB_ID=""
BIOUNIT_NO=""
NUMBER_OF_LINES="$(tail -n +2 $1 | wc -l)"

while IFS='' read -r line || [[ -n "$line" ]]; do

    PDB_ID="$(echo $line | cut -d' ' -f1)"
    BIOUNIT_NO="$(echo $line | cut -d' ' -f2)"
    MIDDLE_OF_PDB_ID="${PDB_ID:1:2}"

    if [ ! -d "$DIR_NAME"/"$MIDDLE_OF_PDB_ID" ]; then
        mkdir "$DIR_NAME"/"$MIDDLE_OF_PDB_ID"
    fi

    if [ -f "$DIR_NAME"/"$MIDDLE_OF_PDB_ID"/"$PDB_ID""_""$BIOUNIT_NO" ]; then
        echo "$line" >> \
             "$DIR_NAME"/"$MIDDLE_OF_PDB_ID"/"$PDB_ID""_""$BIOUNIT_NO" 
    else
        echo -e "pdb_id\tbiounit_no\td1\td1_symmetry\td1_cluster95\t"\
             "d2\td2_symmetry\td2_cluster95\tinteraction_id\t"\
             "interface_id\ti_sequence_cluster_95\ti_cluster_95_50\tarea" > \
             "$DIR_NAME"/"$MIDDLE_OF_PDB_ID"/"$PDB_ID""_""$BIOUNIT_NO" 
        echo "$line" >> \
             "$DIR_NAME"/"$MIDDLE_OF_PDB_ID"/"$PDB_ID""_""$BIOUNIT_NO" 
    fi

    COUNTER=$((COUNTER+1))
    echo "$COUNTER/$NUMBER_OF_LINES" 

done < <(tail -n +2 $1)
