#!/bin/bash

: '''
Usage example:
./clusInfo.sh "Clusters%Y.%m.%d_%H:%M:%S\"

Output:
Every ve[0-9]+cl[0-9]+ folder is filled with ve[0-9]+cl[0-9]+_info.txt file
with basic cluster pruning info.

P.S. It is important to put "\" character while using.
'''

COUNTER=0
NUMBER_OF_COMPLEXES=$(find "$1" -type f | wc -l)

for i in "$1"*/*/*; do
    FILE_NAME="$(echo $i | cut -d'/' -f-3)""/""$(echo $i | cut -d'/' -f3)"`
        `"_info.txt";
    PROTEIN_COMPLEX=$(echo $i | cut -d '/' -f 4);
    ALL_EDGES=$(tail -n +2 "$i" | wc -l);
    UNCUT_EDGES=$(tail -n +2 "$i" | awk '{if ($15 == 0) print $0}' | wc -l);
    CUT_EDGES=$(tail -n +2 "$i" | awk '{if ($15 == 1) print $0}' | wc -l);
    MINIMAL_CUT_EDGE=$(echo $(tail -n +2 $i | awk '{if($15 == 1) print $13}' |\
        sort -n -t'.' -k1,1) | cut -d" " -f1);
    test -z "$MINIMAL_CUT_EDGE" && MINIMAL_CUT_EDGE="NA"
    MAXIMAL_CUT_EDGE=$(echo $(tail -n +2 $i | awk '{if($15 == 1) print $13}' |\
        sort -n -r -t'.' -k1,1) | cut -d" " -f1);
    test -z "$MAXIMAL_CUT_EDGE" && MAXIMAL_CUT_EDGE="NA"
    MINIMAL_UNCUT_EDGE=$(echo $(tail -n +2 $i |\
        awk '{if($15 == 0) print $13}' | sort -n -t'.' -k1,1) | cut -d" " -f1);
    if [ -f "$FILE_NAME" ]; then
        echo -e "$PROTEIN_COMPLEX\t$ALL_EDGES\t$UNCUT_EDGES\t$CUT_EDGES\t"\
            "$MINIMAL_CUT_EDGE\t$MAXIMAL_CUT_EDGE\t$MINIMAL_UNCUT_EDGE" >>\
            $FILE_NAME
    else
        echo -e "protein_complex\tnumber_of_edges_before\t"`
            `"number_of_edges_after\tnumber_of_cut_edges\tminimal_cut_edge\t"`
            `"maximal_cut_edge\tminimal_uncut_edge\n"`
            `"$PROTEIN_COMPLEX\t$ALL_EDGES\t$UNCUT_EDGES\t$CUT_EDGES\t"`
            `"$MINIMAL_CUT_EDGE\t$MAXIMAL_CUT_EDGE\t$MINIMAL_UNCUT_EDGE" >\
            $FILE_NAME;
    fi

    COUNTER=$((COUNTER+1))
    echo "$COUNTER/$NUMBER_OF_COMPLEXES"
done
