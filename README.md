## General info
---
This text lays out the whole course of getting bachelor work's results.

If you are interested in a specific program of this repository you should check
that program's help or docstring because all of them have either one of these. 

R code should be applied by hardcoding input file's name.

## Preparing PPI data
---

Fetch 'protein_interactions_with_areas_no_domains.gz' data file and unzip it by running:

```console
$ gunzip protein_interactions_with_areas_no_domains.gz
```

```console
$ head protein_interactions_with_areas_no_domains

pdb_id	biounit_no	d1	d1_symmetry	d1_cluster95	d2	d2_symmetry	d2_cluster95	interaction_id	interface_id	i_sequence_cluster_95	i_cluster_95_50	area
3vg8	1	3vg8_A	1	36249	3vg8_B	2	36249	726427	302731	37903	4	100.0001
3vg8	1	3vg8_A	2	36249	3vg8_B	1	36249	726428	302731	37903	4	100.0001
3l71	1	3l71_N	1	4520	3l71_Q	1	18332	392520	162216	11330	0	100.0021498
6msb	1	6msb_I	1	16243	6msb_M	1	16870	1058369	465822	4427	0	100.00391
5np6	1	5np6_z	1	43880	5np6_1	1	43139	951084	416354	4974	0	100.004504
6h8k	1	6h8k_1	1	55299	6h8k_e	1	NULL	1062777	468859	NULL	NULL	100.006669
1k9i	1	1k9i_G	1	27561	1k9i_H	1	27561	373898	151642	23687	1	100.00869
4y8p	1	4y8p_D	1	16122	4y8p_B	1	16497	813773	349381	1913	0	100.009
4no9	1	4no9_A	1	17330	4no9_C	1	16915	457840	190702	38	0	100.010318391
```

Detalisation of columns:  

* pdb_id, biounit_no: these two columns define a protein complex;
* d1, d1_symmetry, d1_cluster95: interactor 1 and it's sequence cluster (unique description: d1+d1_symmetry), a node in graph
* d2, d2_symmetry, d2_cluster95: interactor 2 and it's sequence cluster (unique description: d2+d2_symmetry), a node in graph
* interaction_id: protein-protein interaction (edge in graph)
* interface_id: interface ID, all interactions that have the same interface_id are in fact the same
* i_sequence_cluster_95, i_cluster_95_50: interface cluster, same interface cluster means same interaction mode
* area: area of interaction interface (weight of edge in graph)

## Replication of bachelor work
---

Specific protein complexes (e.g. 5lsf_1) could be get by grepping the data:
```console
$ grep -P '5lsf\t1' protein_interactions_with_areas_no_domains
```

But in order to get a directory with all graphs info layed out in separate files run: 

```console
$ bash getAllGraphs.sh protein_interactions_with_areas_no_domains
```
All graphs will be in the newly created directory named "Graphs_%Y.%m.%d_%H:%M:%S\" with corresponding time stamps.

Remove all disconnected graphs from further investigation by doing following commands (adjust number of processors in xargs command -P option according to your computer possibilities which you could check with lscpu command or rewrite commands without nohup if want so): 

```console
$ nohup sh -c 'for i in Graphs_2020.05.10_17\:58\:48/*/*; do echo $i; done | xargs -P 3 -L 1 python nxNac.py -d' &
$ cat nohup.out > disconnectedGraphs.txt
$ for i in $(cat disconnectedGraphs.txt); do rm $i; done
```

You could modify protein complexes file to not include path by doing:
```console
$ cat disconnectedGraphs.txt | cut -d'/' -f3 > someFile.txt
```

Or get whole ppi3d info file without disconnected graphs info:
```console
$ for i in Graphs_2020.05.10_17\:58\:48/*/*; do tail -n +2 $i >> ppi3dNoDisconnected.txt; done
```

With distributed graphs info and removed disconnected graphs main.py and analyseGPD.r could be used to repeat Zhou et al., Network Simplification with Minimal Loss of Connectivity, Proceedings of the 10th IEEE International Conference on Data Mining (ICDM), 659-668, 2010 article with PPI3D data. Use help for further information.

Get graph files with added supplementary info (for rounding add -r option):
```console
$ nohup sh -c 'for i in Graphs_2020.05.10_17\:58\:48/*/*; do echo $i; done | xargs -P 3 -L 1 python nxNac.py -s' &
```

And leave only supplementary files:
```console
$ for i in Graphs_2020.05.10_17\:58\:48/*/*[^.txt]; do rm $i; done
$ for i in Graphs_2020.05.10_17\:58\:48/*/*; do mv $i ${i%.*}; done
```

Clustering files into one directory is done by getting supplementary file with whole PPI3D info without disconnected graphs and executing:
```console
$ python clustering.py ppi3dNoDisconnectedSupplementary.txt
```

After clustering you could get info about all clusters:
```console
$ nohup bash clustInfo.sh Clusters_2020.05.13_00\:30\:51/ &
```

Count the numbers of all, positive and negatice clusters:
```console
$ bash clusterEq.sh Clusters_2020.05.13_00\:30\:39/
```

Get csv files for R analysis of clusters
```console
$ for i in Clusters_2020.05.13_00\:30\:39/*/*/*_info.txt; do echo $i >> fileForNegClusInfo.txt; done
$ nohup bash negClusInfo.sh fileForNegClusInfo.txt ncci.txt &
```

## Postscript
---
* Not all R scripts were added to the repository due to repetitiveness of code and different importance of the results. In case of wanting to get those scripts contact the author.
* Not all programs were mentioned in README (i.e. subgraphs.py, subclusters.py and visualizePruning.py) because they were not used directly for results in bachelor work. Although they could be used for PPI3D data analysis. Read the docstrings for further information.

## Running the tests
---
Execute the following command:
```console
$ python test_main.py
```

## Author
---
* Tautminas Cibulskis
