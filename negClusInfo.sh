#!/bin/bash

: '''
Usage example:
./negClusInfo.sh "negative_connected_clusters.txt" \
"negative_connected_clusters_info.csv"

negative_connected_clusters_info.txt is a list of negative connected clusters.
head -n 5 negative_connected_clusters.txt:
Clusters_2020.02.26_14:17:46_no_disconnected/ver10/ve10cl102/ve10cl102_info.txt
Clusters_2020.02.26_14:17:46_no_disconnected/ver10/ve10cl135/ve10cl135_info.txt
Clusters_2020.02.26_14:17:46_no_disconnected/ver10/ve10cl138/ve10cl138_info.txt
Clusters_2020.02.26_14:17:46_no_disconnected/ver10/ve10cl166/ve10cl166_info.txt
Clusters_2020.02.26_14:17:46_no_disconnected/ver10/ve10cl22/ve10cl22_info.txt

Output:
File negative_connected_clusters_info.csv is filled with info about negative
clusters in csv format
'''

echo "cluster_id,num_of_compl,num_of_ver,is_all_eq,smal_num_edg_bef,"`
     `"big_num_edg_bef,av_num_edg_bef,smal_num_edg_aft,big_num_edg_aft,"`
     `"av_num_edg_aft,smal_num_cut_edg,big_num_cut_edg,av_num_cut_edg,"`
     `"smal_min_cut_edg,big_min_cut_edg,av_min_cut_edg,smal_max_cut_edg,"`
     `"big_max_cut_edg,av_max_cut_edg,smal_un_edg,big_un_edg,av_un_edg" > $2

COUNTER=0
NUMBER_OF_LINES="$(wc -l $1 | cut -d' ' -f 1)"

for i in $(cat $1); do

    CLUSTER_ID=$(echo "$i" | cut -d'/' -f 4 | cut -d'_' -f 1)
    NUM_OF_COMPL=$(tail -n +2 "$i" | wc -l)
    NUM_OF_VER=$(echo "${CLUSTER_ID:2}" | awk -F 'cl' '{print $1}')

    if (( $(tail -n +2 "$i" | awk '{print $2}' | sort -n -u -k 1,1 |\
    wc -l) == 1 )); then
        ALL_EQ=1
    else
        ALL_EQ=0
    fi

    SMALL_NUM_EDG_BEF=$(tail -n +2 "$i" | awk '{print $2}' | sort -n -k 1,1 |\
        cut -d$'\n' -f 1)
    BIG_NUM_EDG_BEF=$(tail -n +2 "$i" | awk '{print $2}' | sort -n -r -k 1,1 |\
        cut -d$'\n' -f 1)
    AV_NUM_EDG_BEF=$(tail -n +2 "$i" |\
        awk '{ total += $2; count++ } END { print total/count }')
    SMALL_NUM_EDG_AFT=$(tail -n +2 "$i" | awk '{print $3}' | sort -n -k 1,1 |\
        cut -d$'\n' -f 1)
    BIG_NUM_EDG_AFT=$(tail -n +2 "$i" | awk '{print $3}' | sort -n -r -k 1,1 |\
        cut -d$'\n' -f 1)
    AV_NUM_EDG_AFT=$(tail -n +2 "$i" |\
        awk '{ total += $3; count++ } END { print total/count }')
    SMALL_NUM_CUT_EDG=$(tail -n +2 "$i" | awk '{print $4}' | sort -n -k 1,1 |\
        cut -d$'\n' -f 1)
    BIG_NUM_CUT_EDG=$(tail -n +2 "$i" | awk '{print $4}' | sort -n -r -k 1,1 |\
        cut -d$'\n' -f 1)
    AV_NUM_CUT_EDG=$(tail -n +2 "$i" |\
        awk '{ total += $4; count++ } END { print total/count }')
    SMALL_MIN_CUT_EDG=$(tail -n +2 "$i" | awk '{if ($5 != "NA") print $5}' |\
        sort -n -t'.' -k 1,1 | cut -d$'\n' -f 1)
    BIG_MIN_CUT_EDG=$(tail -n +2 "$i" | awk '{if ($5 != "NA") print $5}' |\
        sort -n -t'.' -r -k 1,1 | cut -d$'\n' -f 1)
    AV_MIN_CUT_EDG=$(tail -n +2 "$i" |\
        awk '{if ($5 != "NA") {total += $5; count++} } END { if (count != 0) {print total/count} }')
    SMALL_MAX_CUT_EDG=$(tail -n +2 "$i" |\
        awk '{if ($6 != "NA") print $6}' | sort -n -t'.' -k 1,1 |\
        cut -d$'\n' -f 1)
    BIG_MAX_CUT_EDG=$(tail -n +2 "$i" | awk '{if ($6 != "NA") print $6}' |\
        sort -n -t'.' -r -k 1,1 | cut -d$'\n' -f 1)
    AV_MAX_CUT_EDG=$(tail -n +2 "$i" |\
        awk '{ if ($6 != "NA") {total += $6; count++} } END { if (count != 0) {print total/count} }')
    SMALL_UNCUT_EDG=$(tail -n +2 "$i" | awk '{if ($7 != "NA") print $7}' |\
        sort -n -t'.' -k 1,1 | cut -d$'\n' -f 1)
    BIG_UNCUT_EDG=$(tail -n +2 "$i" | awk '{if ($7 != "NA") print $7}' |\
        sort -n -t'.' -r -k 1,1 | cut -d$'\n' -f 1)
    AV_UNCUT_EDG=$(tail -n +2 "$i" |\
        awk '{if ($7 != "NA") {total += $7; count++} } END { if (count != 0) {print total/count} }')

    echo "$CLUSTER_ID,$NUM_OF_COMPL,$NUM_OF_VER,$ALL_EQ,$SMALL_NUM_EDG_BEF,"`
         `"$BIG_NUM_EDG_BEF,$AV_NUM_EDG_BEF,$SMALL_NUM_EDG_AFT,"`
         `"$BIG_NUM_EDG_AFT,$AV_NUM_EDG_AFT,$SMALL_NUM_CUT_EDG,"`
         `"$BIG_NUM_CUT_EDG,$AV_NUM_CUT_EDG,$SMALL_MIN_CUT_EDG,"`
         `"$BIG_MIN_CUT_EDG,$AV_MIN_CUT_EDG,$SMALL_MAX_CUT_EDG,"`
         `"$BIG_MAX_CUT_EDG,$AV_MAX_CUT_EDG,$SMALL_UNCUT_EDG,"`
         `"$BIG_UNCUT_EDG,$AV_UNCUT_EDG" >> $2

    COUNTER=$((COUNTER+1))
    echo "$COUNTER/$NUMBER_OF_LINES"

done
