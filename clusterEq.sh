#!/bin/bash

: '''
Usage example (folder has to have supplementary info files):
./clusterEq.sh "Clusters%Y.%m.%d_%H:%M:%S\"

Output:
Number of clusters which have same number of edges (positive),
number of negative clusters and all clusters
'''

ALL_CLUST=0
ANA_CLUST=0
POS_CLUST=0
NEG_CLUST=0

for i in $1*/*/*_info.txt; do
    ALL_CLUST=$(($ALL_CLUST+1))
    IS_CUT=$(tail -n +2 $i | cut -f 4 | grep '^[1-9]')
    # Checking if at least one edge is cut
    if (( ${#IS_CUT} > 0 )); then
        # Checking if cluster has more than one element
        if (( $(cat $i | wc -l) > 2 )); then
            ANA_CLUST=$(($ANA_CLUST+1))
            echo "Number of analysed clusters: $ANA_CLUST, cluster: $i"
            DIFF_EDG=$(tail -n +2 $i | cut -f 3 | sort -u | wc -l)
            # Checking if pruned graphs have the same number of edges
            if (( $DIFF_EDG > 1 )); then
                NEG_CLUST=$(($NEG_CLUST+1))
            else
                POS_CLUST=$(($POS_CLUST+1))
            fi
        fi
    fi
done

echo "RESULTS:"
echo "Number of all complexes: $ALL_CLUST"
echo "Number of positive clusters: $POS_CLUST"
echo "Number of negative clusters: $NEG_CLUST"