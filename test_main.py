import unittest
import main
from main import Graph


class TestMain(unittest.TestCase):

    def test_naive_approach(self):
        g = {"a": {"c": [9, None]},
             "b": {"c": [5, None], "e": [2, None], "f": [7, None]},
             "c": {"a": [9, None], "b": [5, None], "d": [3, None],
                   "e": [7, None]},
             "d": {"c": [3, None]},
             "e": {"c": [7, None], "b": [2, None], "f": [10, None]},
             "f": {"e": [10, None], "b": [7, None]}}
        graph = Graph(g)
        graph = graph.naive_approach(1)
        result = {"a": {"c": [9, None]},
                  "c": {"a": [9, None], "e": [7, None], "d": [3, None]},
                  "b": {"f": [7, None]},
                  "e": {"c": [7, None], "f": [10, None]},
                  "d": {"c": [3, None]},
                  "f": {"b": [7, None], "e": [10, None]}}
        rgraph = Graph(result)
        assert graph == rgraph

        g = {"a": {"c": [9, None]},
             "b": {"c": [5, None], "e": [10, None], "f": [7, None]},
             "c": {"a": [9, None], "b": [5, None], "d": [3, None],
                   "e": [7, None]},
             "d": {"c": [3, None]},
             "e": {"c": [7, None], "b": [10, None], "f": [1, None]},
             "f": {"e": [1, None], "b": [7, None]}}
        graph = Graph(g)
        graph = graph.naive_approach(1)
        result = {"a": {"c": [9, None]},
                  "c": {"a": [9, None], "e": [7, None], "d": [3, None]},
                  "b": {"e": [10, None], "f": [7, None]},
                  "e": {"c": [7, None], "b": [10, None]},
                  "d": {"c": [3, None]},
                  "f": {"b": [7, None]}}

        rgraph = Graph(result)
        assert graph == rgraph

    def test_brute_force_approach(self):
        g = {"a": {"b": [0.78, None], "c": [0.63, None], "d": [0.5, None],
                   "e": [0.5, None], "f": [0.5, None], "g": [0.5, None],
                   "h": [0.5, None], "i": [0.5, None], "j": [0.5, None]},
             "b": {"a": [0.78, None], "c": [0.7, None]},
             "c": {"a": [0.63, None], "b": [0.7, None]},
             "d": {"a": [0.5, None]},
             "e": {"a": [0.5, None]},
             "f": {"a": [0.5, None]},
             "g": {"a": [0.5, None]},
             "h": {"a": [0.5, None]},
             "i": {"a": [0.5, None]},
             "j": {"a": [0.5, None]}}

        graph = Graph(g)
        graph = graph.brute_force_approach(1)
        result = {"a": {"c": [0.63, None], "b": [0.78, None], "e": [0.5, None],
                        "d": [0.5, None], "g": [0.5, None], "f": [0.5, None],
                        "i": [0.5, None], "h": [0.5, None], "j": [0.5, None]},
                  "c": {"a": [0.63, None]},
                  "b": {"a": [0.78, None]},
                  "e": {"a": [0.5, None]},
                  "d": {"a": [0.5, None]},
                  "g": {"a": [0.5, None]},
                  "f": {"a": [0.5, None]},
                  "i": {"a": [0.5, None]},
                  "h": {"a": [0.5, None]},
                  "j": {"a": [0.5, None]}}
        rgraph = Graph(result)
        assert graph == rgraph

        g = {"a": {"b": [0.64, None], "d": [0.9, None], "e": [0.5, None],
                   "f": [0.5, None], "g": [0.5, None], "h": [0.5, None]},
             "b": {"a": [0.64, None], "c": [0.77, None]},
             "c": {"b": [0.77, None], "d": [0.8, None]},
             "d": {"a": [0.9, None], "c": [0.8, None], "i": [0.5, None],
                   "j": [0.5, None]},
             "e": {"a": [0.5, None]},
             "f": {"a": [0.5, None]},
             "g": {"a": [0.5, None]},
             "h": {"a": [0.5, None]},
             "i": {"d": [0.5, None]},
             "j": {"d": [0.5, None]}}

        graph = Graph(g)
        graph = graph.brute_force_approach(1)
        result = {"a": {"b": [0.64, None], "e": [0.5, None], "d": [0.9, None],
                        "g": [0.5, None], "f": [0.5, None], "h": [0.5, None]},
                  "c": {"d": [0.8, None]},
                  "b": {"a": [0.64, None]},
                  "e": {"a": [0.5, None]},
                  "d": {"a": [0.9, None], "i": [0.5, None], "c": [0.8, None],
                        "j": [0.5, None]},
                  "g": {"a": [0.5, None]},
                  "f": {"a": [0.5, None]},
                  "i": {"d": [0.5, None]},
                  "h": {"a": [0.5, None]},
                  "j": {"d": [0.5, None]}}
        rgraph = Graph(result)
        assert graph == rgraph

    def test_path_simplification(self):
        g = {"a": {"b": [0.78, None], "c": [0.63, None], "d": [0.5, None],
                   "e": [0.5, None], "f": [0.5, None], "g": [0.5, None],
                   "h": [0.5, None], "i": [0.5, None], "j": [0.5, None]},
             "b": {"a": [0.78, None], "c": [0.7, None]},
             "c": {"a": [0.63, None], "b": [0.7, None]},
             "d": {"a": [0.5, None]},
             "e": {"a": [0.5, None]},
             "f": {"a": [0.5, None]},
             "g": {"a": [0.5, None]},
             "h": {"a": [0.5, None]},
             "i": {"a": [0.5, None]},
             "j": {"a": [0.5, None]}}

        graph = Graph(g)
        graph = graph.path_simplification(1)
        result = {"a": {"b": [0.78, None], "e": [0.5, None], "d": [0.5, None],
                        "g": [0.5, None], "f": [0.5, None], "i": [0.5, None],
                        "h": [0.5, None], "j": [0.5, None]},
                  "c": {"b": [0.7, None]},
                  "b": {"a": [0.78, None], "c": [0.7, None]},
                  "e": {"a": [0.5, None]}, "d": {"a": [0.5, None]},
                  "g": {"a": [0.5, None]},
                  "f": {"a": [0.5, None]},
                  "i": {"a": [0.5, None]},
                  "h": {"a": [0.5, None]},
                  "j": {"a": [0.5, None]}}
        rgraph = Graph(result)
        assert graph == rgraph

        g = {"a": {"b": [0.64, None], "d": [0.9, None], "e": [0.5, None],
                   "f": [0.5, None], "g": [0.5, None], "h": [0.5, None]},
             "b": {"a": [0.64, None], "c": [0.77, None]},
             "c": {"b": [0.77, None], "d": [0.8, None]},
             "d": {"a": [0.9, None], "c": [0.8, None], "i": [0.5, None],
                   "j": [0.5, None]},
             "e": {"a": [0.5, None]},
             "f": {"a": [0.5, None]},
             "g": {"a": [0.5, None]},
             "h": {"a": [0.5, None]},
             "i": {"d": [0.5, None]},
             "j": {"d": [0.5, None]}}

        graph = Graph(g)
        graph = graph.path_simplification(1)
        result = {"a": {"e": [0.5, None], "d": [0.9, None], "g": [0.5, None],
                        "f": [0.5, None], "h": [0.5, None]},
                  "c": {"b": [0.77, None], "d": [0.8, None]},
                  "b": {"c": [0.77, None]},
                  "e": {"a": [0.5, None]},
                  "d": {"a": [0.9, None], "i": [0.5, None], "c": [0.8, None],
                        "j": [0.5, None]},
                  "g": {"a": [0.5, None]},
                  "f": {"a": [0.5, None]},
                  "i": {"d": [0.5, None]},
                  "h": {"a": [0.5, None]},
                  "j": {"d": [0.5, None]}}
        rgraph = Graph(result)
        assert graph == rgraph

    def test_combinational_approach(self):
        g = {"a": {"b": [0.78, None], "c": [0.63, None], "d": [0.5, None],
                   "e": [0.5, None], "f": [0.5, None], "g": [0.5, None],
                   "h": [0.5, None], "i": [0.5, None], "j": [0.5, None]},
             "b": {"a": [0.78, None], "c": [0.7, None]},
             "c": {"a": [0.63, None], "b": [0.7, None]},
             "d": {"a": [0.5, None]},
             "e": {"a": [0.5, None]},
             "f": {"a": [0.5, None]},
             "g": {"a": [0.5, None]},
             "h": {"a": [0.5, None]},
             "i": {"a": [0.5, None]},
             "j": {"a": [0.5, None]}}

        graph = Graph(g)
        graph = graph.combinational_approach(1)
        result = {"a": {"b": [0.78, None], "e": [0.5, None], "d": [0.5, None],
                        "g": [0.5, None], "f": [0.5, None], "i": [0.5, None],
                        "h": [0.5, None], "j": [0.5, None]},
                  "c": {"b": [0.7, None]},
                  "b": {"a": [0.78, None], "c": [0.7, None]},
                  "e": {"a": [0.5, None]}, "d": {"a": [0.5, None]},
                  "g": {"a": [0.5, None]},
                  "f": {"a": [0.5, None]},
                  "i": {"a": [0.5, None]},
                  "h": {"a": [0.5, None]},
                  "j": {"a": [0.5, None]}}
        rgraph = Graph(result)
        assert graph == rgraph


if __name__ == '__main__':
    unittest.main()
