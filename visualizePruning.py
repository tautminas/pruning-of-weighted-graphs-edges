'''
Usage example:
python visualizePruning.py 1boc_1 3ldi_1

head -n 2 1boc_1 (\ used for line wraps):
pdb_id	biounit_no	d1	d1_symmetry	d1_cluster95	d2 \
d2_symmetry	d2_cluster95	interaction_id	interface_id \
i_sequence_cluster_95	i_cluster_95_50	area	complex_id	to_be_pruned \
betweenness_centrality
1b0c	1	1b0c_D	1	43187	1b0c_C	2	43187	57100	25286 \
1416	6	201.279478	1b0c_1	0	0.177777777778

Output (in directory where 1boc_1 and 3ldi_1 are placed):
1boc_1.svg
3ldi_1.svg
'''

import matplotlib.pyplot as plt
import itertools
import argparse
import random
import string
import math
import sys

parser = argparse.ArgumentParser(description='Parameters for graph pruning ' +
                                 'visualization.')
parser.add_argument('file', metavar='N', type=str, nargs='+',
                    help='Suplementary protein complex file')
parser.add_argument('-o', '--optimize', action='store_true', dest='o',
                    help='X axis ticks are not ploted for faster results')
parser.add_argument('-l', '--lines', action='store_true', dest='l',
                    help='Horizontal lines are drawn at 400A and 500A')
args = parser.parse_args()

allEdgCol = []
tmpNrOfEdgCol = {}
tmpMinOfEdgCol = {}
nrOfEdgCol = {}
minOfEdgCol = {}
biggestArea = 0
# First read of protein files during which number of columns, their order and
# Y axis is determined.
for i in args.file:
    fh = open(i, "r")
    fh.readline()
    for line in fh:
        line = line.split()
        edgCol = line[10] + "_" + line[11]

        if biggestArea < float(line[12]):
            biggestArea = float(line[12])

        if edgCol not in allEdgCol:
            allEdgCol.append(edgCol)

        if edgCol not in tmpMinOfEdgCol:
            tmpMinOfEdgCol[edgCol] = [float(line[12])]
        else:
            tmpMinOfEdgCol[edgCol].append(float(line[12]))

        if edgCol not in tmpNrOfEdgCol:
            tmpNrOfEdgCol[edgCol] = 1
        else:
            tmpNrOfEdgCol[edgCol] += 1

    for j in tmpMinOfEdgCol:
        tmpMinOfEdgCol[j] = min(tmpMinOfEdgCol[j])
        if j not in minOfEdgCol:
            minOfEdgCol[j] = tmpMinOfEdgCol[j]
        elif minOfEdgCol[j] > tmpMinOfEdgCol[j]:
            minOfEdgCol[j] = tmpMinOfEdgCol[j]

    for j in tmpNrOfEdgCol:
        if j not in nrOfEdgCol:
            nrOfEdgCol[j] = tmpNrOfEdgCol[j]
        elif nrOfEdgCol[j] < tmpNrOfEdgCol[j]:
            nrOfEdgCol[j] = tmpNrOfEdgCol[j]

    tmpMinOfEdgCol = {}
    tmpNrOfEdgCol = {}

    allEdgColMinAr = []
    allEdgColMaxNr = []
    for i in allEdgCol:
        allEdgColMinAr.append(minOfEdgCol[i])
        allEdgColMaxNr.append(nrOfEdgCol[i])

    lists = sorted(itertools.izip(*[allEdgColMinAr, allEdgColMaxNr,
                                    allEdgCol]))
    allEdgColMinAr, allEdgColMaxNr, allEdgCol = list(itertools.izip(*lists))

    allEdgColMinAr = list(allEdgColMinAr)
    allEdgColMaxNr = list(allEdgColMaxNr)
    allEdgCol = list(allEdgCol)

emptyProtNr = 1
# Second read of protein files during which info for drawing is collected.
for protName in args.file:
    area = []
    tbp = []
    int_id = []
    edg_col = []

    fh = open(protName, "r")
    fh.readline()
    for line in fh:
        line = line.split()
        int_id.append(line[8])
        area.append(float(line[12]))
        tbp.append(int(line[14]))
        edg_col.append(line[10] + "_" + line[11])

    finalArea = []
    finalTbp = []
    finalInId = []
    for i in range(len(allEdgCol)):
        nrOfApp = 0
        for j in range(len(edg_col)):
            if edg_col[j] == allEdgCol[i]:
                nrOfApp += 1
                finalArea.append(area[j])
                finalTbp.append(tbp[j])
                finalInId.append(int_id[j])
        while nrOfApp != allEdgColMaxNr[i]:
                nrOfApp += 1
                finalArea.append(0)
                finalTbp.append(0)
                finalInId.append("emptyProtNr: " + str(emptyProtNr))
                emptyProtNr += 1
    colours = plt.bar(finalInId, finalArea)
    for i in range(0, len(finalTbp)):
        if finalTbp[i] == 1:
            colours[i].set_color('r')
        else:
            colours[i].set_color('b')

    # Setting Y axis 110% bigger than biggest area.
    axes = plt.gca()
    axes.set_ylim([0, math.ceil(1.10 * biggestArea)])

    plt.gcf().canvas.set_window_title("Protein complex: " + line[13])
    plt.xlabel("Interaction ID")
    plt.ylabel("Area")

    if args.o is True:
        plt.xlabel("")
        plt.xticks([])
    else:
        plt.tick_params(labelbottom=False)

    if args.l:
        plt.axhline(y=500, color='black', linestyle='-')
        plt.axhline(y=400, color='red', linestyle='--')

    plt.savefig(protName + ".svg", format = "svg")
    plt.close()
