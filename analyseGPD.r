# Read csv file.
gpd <- read.csv("C:/Users/ATDL/Desktop/graph_pruning_summary.csv",
                stringsAsFactors=FALSE)

# Change NA values to usable ones shortened "Naive Approach" values.
gpd$Algorithm[is.na(gpd$Algorithm)] <- "NaAp"
# Reorder algorithms as factors levels for visualization.
gpd$Algorithm <- as.factor(gpd$Algorithm)
gpd$Algorithm <- factor(gpd$Algorithm, levels(gpd$Algorithm)[c(3,1,4,2)])

# Variables for the tasks.
nodesCorrespondance <- vector()
gammasCorrespondance <- vector()
gammas <- c(0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1.0)
nodes <- c(3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24)

# Subsets.
gpdPruned <- subset(gpd, gpd$Running_time != "None")
gpdPrunedNA <- subset(gpdPruned, gpdPruned$Algorithm == "NaAp")
gpdPrunedBF <- subset(gpdPruned, gpdPruned$Algorithm == "BF")
gpdPrunedPS <- subset(gpdPruned, gpdPruned$Algorithm == "PS")
gpdPrunedCB <- subset(gpdPruned, gpdPruned$Algorithm == "CB")

# Remake of Fig.1.
boxplot(1-(gpdPruned$Number_of_edges_after/gpdPruned$Number_of_edges_before)~
          gpdPruned$Gama,
        main="Fig.1, Subset: at least one pruned edge",
        ylab="Fraction of edges removed", xlab=expression(gamma),
        ylim=c(0.0,1.0))
axis(2, seq(0.1,0.9,0.2), labels=F)

# Remake of Fig.2.
for(gamma in gammas){
  gpdPrunedNAGama <- subset(gpdPrunedNA, gpdPrunedNA$Gama==gamma)
  gammasCorrespondance <- c(gammasCorrespondance, mean(gpdPrunedNAGama$rk))
}
plot(gammas, gammasCorrespondance, type ="o", col="red", ylim=c(0.65,1),
     main="Fig.2 (Modified: Means of rk kept by algorithms),
     Subset: at least one pruned edge.", ylab="Ratio of connectivity kept",
     xlab=expression(gamma), pch=15)
gammasCorrespondance <- vector()

for(gamma in gammas){
  gpdPrunedBFGama <- subset(gpdPrunedBF, gpdPrunedBF$Gama==gamma)
  gammasCorrespondance <- c(gammasCorrespondance, mean(gpdPrunedBFGama$rk))
}
lines(gammas, gammasCorrespondance, type ="o", col="green", pch=18)
gammasCorrespondance <- vector()

for(gamma in gammas){
  gpdPrunedPSGama <- subset(gpdPrunedPS, gpdPrunedPS$Gama==gamma)
  gammasCorrespondance <- c(gammasCorrespondance, mean(gpdPrunedPSGama$rk))
}
lines(gammas, gammasCorrespondance, type ="o", col="blue", pch=16)
gammasCorrespondance <- vector()

for(gamma in gammas){
  gpdPrunedCBGama <- subset(gpdPrunedCB, gpdPrunedCB$Gama==gamma)
  gammasCorrespondance <- c(gammasCorrespondance, mean(gpdPrunedCBGama$rk))
}
lines(gammas, gammasCorrespondance, type ="o", col="yellow3", pch=17)
gammasCorrespondance <- vector()

axis(1, seq(0.1,0.9,0.2), labels=T)

legend("bottomleft", c("NaAp","BF","PS","CB"), lty=1, pch = c(15,18,16,17),
       col=c('red','green','blue','yellow3'))

# Remake of Fig.3.
for(gamma in gammas){
  gpdPrunedGama <- subset(gpdPruned, gpdPruned$Gama==gamma)
  boxplot(gpdPrunedGama$rk~gpdPrunedGama$Algorithm, ylab="rk",
          xlab="Algorithm", ylim=c(0.0,1.0))
  title(main=bquote("Fig.3, Subset:" ~ gamma == .(gamma) ~
                      "and at least one pruned edge"))
  axis(2, seq(0.1,0.9,0.2), labels=F)
}

# Remake of Fig.5.
for(gamma in gammas){
  gpdPrunedNAGama <- subset(gpdPrunedNA, gpdPrunedNA$Gama==gamma)
  gammasCorrespondance <- c(gammasCorrespondance,
                            mean(as.numeric(gpdPrunedNAGama$Running_time)))
}
plot(gammas, gammasCorrespondance,
     main="Fig.5, Subset: at least one pruned edge.",
     type ="o", col="red", ylim=c(0.001,100), log="y", yaxt = "n",
     ylab="Runtime (second)", xlab=expression(gamma), pch=15)
gammasCorrespondance <- vector()

for(gamma in gammas){
  gpdPrunedBFGama <- subset(gpdPrunedBF, gpdPrunedBF$Gama==gamma)
  gammasCorrespondance <- c(gammasCorrespondance, 
                            mean(as.numeric(gpdPrunedBFGama$Running_time)))
}
lines(gammas, gammasCorrespondance, type ="o", col="green", pch=18)
gammasCorrespondance <- vector()

for(gamma in gammas){
  gpdPrunedPSGama <- subset(gpdPrunedPS, gpdPrunedPS$Gama==gamma)
  gammasCorrespondance <- c(gammasCorrespondance, 
                            mean(as.numeric(gpdPrunedPSGama$Running_time)))
}
lines(gammas,gammasCorrespondance,type ="o",col="blue",pch=16)
gammasCorrespondance <- vector()

for(gamma in gammas){
  gpdPrunedCBGama <- subset(gpdPrunedCB, gpdPrunedCB$Gama==gamma)
  gammasCorrespondance <- c(gammasCorrespondance, 
                            mean(as.numeric(gpdPrunedCBGama$Running_time)))
}
lines(gammas, gammasCorrespondance, type ="o", col="yellow3", pch=17)
gammasCorrespondance <- vector()

axis(1, seq(0.1,0.9,0.2), labels=T)
axis(2, c(0.001,0.01,0.1,1,10,100),
     labels=c("0.001","0.01","0.1","1","10","100"))
axis(4,c(0.001,0.01,0.1,1,10,100),
     labels=c("0.001","0.01","0.1","1","10","100"))

legend("topleft", c("NaAp","BF","PS","CB"), lty=1, pch = c(15,18,16,17),
       col=c('red','green','blue','yellow3'), cex=0.86)

# Remake of Fig.7.
for(node in nodes){
  gpdPrunedNANode <- subset(gpdPrunedNA, gpdPrunedNA$Number_of_vertices==node &
                              gpdPrunedNA$Gama==0.4)
  nodesCorrespondance <- c(nodesCorrespondance,
                           mean(as.numeric(gpdPrunedNANode$Running_time)))
}
plot(nodes, nodesCorrespondance, type ="o", col="red", ylim=c(0,0.06),
     ylab="Runtime (second)", xlab="Number of nodes", lty="solid", pch=15,
     main=bquote("Fig.7, Subset: " ~ 
                   gamma ~ " = 0.4 and at least one pruned edge."))
nodesCorrespondance <- vector()

for(node in nodes){
  gpdPrunedPSNode <- subset(gpdPrunedPS, gpdPrunedPS$Number_of_vertices==node &
                              gpdPrunedPS$Gama==0.4)
  nodesCorrespondance <- c(nodesCorrespondance,
                           mean(as.numeric(gpdPrunedPSNode$Running_time)))
}
lines(nodes,nodesCorrespondance,type ="o",col="blue",lty="dashed",pch=16)
nodesCorrespondance <- vector()

for(node in nodes){
  gpdPrunedCBNode <- subset(gpdPrunedCB, gpdPrunedCB$Number_of_vertices==node &
                              gpdPrunedCB$Gama==0.4)
  nodesCorrespondance <- c(nodesCorrespondance,
                           mean(as.numeric(gpdPrunedCBNode$Running_time)))
}
lines(nodes, nodesCorrespondance, type ="o", col="yellow3", lty="dotted",
      pch=17)
nodesCorrespondance <- vector()

axis(1, c(3,24), labels=T)

legend("topleft",c("NaAp","PS","CB"), lty=c("solid","dashed","dotted"), 
       pch = c(15,16,17), col=c('red','blue','yellow3'))

# Remake of Fig.8.
for(node in nodes){
  gpdPrunedNANode <- subset(gpdPrunedNA, gpdPrunedNA$Number_of_vertices==node &
                              gpdPrunedNA$Gama==0.8)
  nodesCorrespondance <- c(nodesCorrespondance,
                           mean(as.numeric(gpdPrunedNANode$Running_time)))
}
plot(nodes, nodesCorrespondance, type ="o", col="red", ylim=c(0,0.06),
     ylab="Runtime (second)", xlab="Number of nodes", lty="solid", pch=15,
     main=bquote("Fig.8, Subset: " ~ 
                   gamma ~ " = 0.8 and at least one pruned edge."))
nodesCorrespondance <- vector()

for(node in nodes){
  gpdPrunedPSNode <- subset(gpdPrunedPS, gpdPrunedPS$Number_of_vertices==node &
                              gpdPrunedPS$Gama==0.8)
  nodesCorrespondance <- c(nodesCorrespondance, 
                           mean(as.numeric(gpdPrunedPSNode$Running_time)))
}
lines(nodes,nodesCorrespondance, type ="o", col="blue", lty="dashed", pch=16)
nodesCorrespondance <- vector()

for(node in nodes){
  gpdPrunedCBNode <- subset(gpdPrunedCB, gpdPrunedCB$Number_of_vertices==node &
                              gpdPrunedCB$Gama==0.8)
  nodesCorrespondance <- c(nodesCorrespondance, 
                           mean(as.numeric(gpdPrunedCBNode$Running_time)))
}
lines(nodes,nodesCorrespondance, type ="o", col="yellow3", lty="dotted", pch=17)
nodesCorrespondance <- vector()

axis(1, c(3,24), labels=T)

legend("topleft",c("NaAp","PS","CB"), lty=c("solid","dashed","dotted"),
       pch = c(15,16,17), 
       col=c('red','blue','yellow3'))

# Addition to Fig.7 and Fig.8.
for(node in nodes){
  gpdPrunedNANode <- subset(gpdPrunedNA, gpdPrunedNA$Number_of_vertices==node)
  nodesCorrespondance <- c(nodesCorrespondance, 
                           mean(as.numeric(gpdPrunedNANode$Running_time)))
}
plot(nodes, nodesCorrespondance, type ="o", col="red", ylim=c(0.0001,400),
     log="y", yaxt = "n", ylab="Runtime (second)", xlab="Number of nodes",
     pch=15, main="Fig.7+8, Subset: at least one pruned edge.")
nodesCorrespondance <- vector()

for(node in nodes){
  gpdPrunedBFNode <- subset(gpdPrunedBF, gpdPrunedBF$Number_of_vertices==node)
  nodesCorrespondance <- c(nodesCorrespondance,
                           mean(as.numeric(gpdPrunedBFNode$Running_time)))
}
lines(nodes,nodesCorrespondance,type ="o",col="green",pch=18)
nodesCorrespondance <- vector()

for(node in nodes){
  gpdPrunedPSNode <- subset(gpdPrunedPS, gpdPrunedPS$Number_of_vertices==node)
  nodesCorrespondance <- c(nodesCorrespondance,
                           mean(as.numeric(gpdPrunedPSNode$Running_time)))
}
lines(nodes,nodesCorrespondance,type ="o",col="blue",pch=16)
nodesCorrespondance <- vector()

for(node in nodes){
  gpdPrunedCBNode <- subset(gpdPrunedCB, gpdPrunedCB$Number_of_vertices==node)
  nodesCorrespondance <- c(nodesCorrespondance,
                           mean(as.numeric(gpdPrunedCBNode$Running_time)))
}
lines(nodes,nodesCorrespondance,type ="o",col="yellow3",pch=17)
nodesCorrespondance <- vector()

axis(1, c(3,24), labels=T)
axis(2, c(0.0001,0.001,0.01,0.1,1,10,100), 
     labels=c("1e-04","1e-03","0.01","0.1","1","10","100"), cex.axis=0.96)
legend("topleft", c("NaAp","BF","PS","CB"), lty=1, pch = c(15,18,16,17), 
       col=c('red','green','blue','yellow3'))

# Simple histogram
options(scipen=999)
hist(gpdPruned$Quantity_of_colours_before-gpdPruned$Quantity_of_colours_after,
     main="Histogram for difference of colours quantity", 
     xlab="Quantity before - quantity after", xlim=c(0,15), breaks=100)
