'''
Usage example:
python subgraphs.py ppi3d subgraphsForNeo4j.csv

head -n 2 ppi3d (\ used for line breaks):
pdb_id	biounit_no	d1	d1_symmetry	d1_cluster95	d2\
d2_symmetry	d2_cluster95	interaction_id	interface_id\
i_sequence_cluster_95	i_cluster_95_50	area	complex_id\
to_be_pruned	betweenness_centrality
6b0i	1	6b0i_B	1	4553	6b0i_B	2	4553	1012930	444295\
40735	0	403.86239669	6b0i_1	0	0.0003663003663

Output:
subgraphsForNeo4j.csv is created
'''

from datetime import datetime
import logging
import sys
import os
logging.basicConfig(level=logging.INFO)

# Gathering info to dictionaries
complexInfo = {}
complexVertCol = {}
complexIDs = []
with open(sys.argv[1], "r") as ifh:
    ifh.readline()
    edge = ifh.readline().split()
    while edge:
        complexID = str(edge[0]) + "_" + str(edge[1])
        colour1 = str(edge[4])
        colour2 = str(edge[7])
        if complexID in complexVertCol:
            complexVertCol[complexID].append(colour1)
            complexVertCol[complexID].append(colour2)
        else:
            complexVertCol[complexID] =\
                [colour1, colour2]
        if complexID in complexInfo:
            complexInfo[complexID].append(edge)
        else:
            complexInfo[complexID] = [edge]
        edge = ifh.readline().split()
ifh.close()

# Changing complexVertCol dictionary values for subgraphs searching
for i in complexVertCol:
    complexVertCol[i] = set(complexVertCol[i])

ofh = open(sys.argv[2], "w")
ofh.write("name1,colours1,size1,name2,colours2,size2" + "\n")
ofh.close()
# Storing subgraphs
complexCount = 1
for i in complexVertCol:
    for j in complexVertCol:
        if complexVertCol[j].issubset(complexVertCol[i]):
            ofh = open(sys.argv[2], "a")
            ofh.write(i + ',"' + str(list(complexVertCol[i])) + '",' +
                      str(str(list(complexVertCol[i])).count(',') + 1) + ',' +
                      j + ',"' + str(list(complexVertCol[j])) + '",' +
                      str(str(list(complexVertCol[j])).count(',') + 1) + '\n')
    logging.info("%s complexes clustered out of 112396", complexCount)
    complexCount += 1
ofh.close()
