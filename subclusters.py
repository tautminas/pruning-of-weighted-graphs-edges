from datetime import datetime
import argparse
import logging
import copy
logging.basicConfig(level=logging.INFO)

parser = argparse.ArgumentParser(description='Parameters for creating ' +
                                 'subclusters csv file to analyse it with ' +
                                 'neo4j')
parser.add_argument('input_file', metavar='input', type=str, nargs=1,
                    help='Input file i.e. ' +
                    '\'protein_interactions_with_areas_no_domains\' or its ' +
                    'supplementary file')
parser.add_argument('output_file', metavar='output', type=str, nargs=1,
                    help='Output file e.g. \'subclustersForNeo4j.csv\'')
parser.add_argument('-l', '--list', action='store_true', dest='l',
                    help='Interpret colours as a list instead of a set')
args = parser.parse_args()

# Gathering info to dictionaries
complexVertCol = {}
with open(args.input_file[0], "r") as ifh:
    ifh.readline()
    edge = ifh.readline().split()
    while edge:
        complexID = str(edge[0]) + "_" + str(edge[1])
        vertex1 = str(edge[2]) + "_" + str(edge[3])
        colour1 = str(edge[4])
        vertex2 = str(edge[5]) + "_" + str(edge[6])
        colour2 = str(edge[7])
        if complexID in complexVertCol:
            complexVertCol[complexID][0].append(vertex1)
            complexVertCol[complexID][0].append(vertex2)
            complexVertCol[complexID][1].append(colour1)
            complexVertCol[complexID][1].append(colour2)
        else:
            complexVertCol[complexID] =\
                [[vertex1, vertex2], [colour1, colour2]]
        edge = ifh.readline().split()
ifh.close()

if args.l is True:
    complexVertColBaUp = copy.deepcopy(complexVertCol)

# Changing complexVertCol dictionary values for clusters searching
for i in complexVertCol:
    complexVertCol[i][0] = len(set(complexVertCol[i][0]))
    complexVertCol[i][1] = set(complexVertCol[i][1])

# Creating directories according to clusters unique IDs (number of vertices
# and its number according to vertices) and writing files into it
complexCount = 1
verClustCount = {}
clustCol = {}
while complexVertCol:
    oneGroup = []
    complexesToDel = []
    numOfVer = None
    setOfCol = None
    for i in complexVertCol:
        if not oneGroup:
            oneGroup.append(i)
            numOfVer = complexVertCol[i][0]
            setOfCol = complexVertCol[i][1]
            complexesToDel.append(i)
            logging.info("%s complexes clustered out of 112396", complexCount)
            complexCount += 1
        else:
            if complexVertCol[i][0] == numOfVer and\
               complexVertCol[i][1] == setOfCol:
                oneGroup.append(i)
                complexesToDel.append(i)
                logging.info("%s complexes clustered out of 112396",
                             complexCount)
                complexCount += 1
    for i in complexesToDel:
        del complexVertCol[i]
    if numOfVer in verClustCount:
        verClustCount[numOfVer] += 1
    else:
        verClustCount[numOfVer] = 1
    if args.l is False:
        clustCol["ve" + str(numOfVer) + "cl" + str(verClustCount[numOfVer])] =\
            setOfCol
    else:
        listOfVer = complexVertColBaUp[oneGroup[0]][0]
        listOfCol = complexVertColBaUp[oneGroup[0]][1]
        dictOfCol = {}
        for i in range(len(listOfVer)):
            dictOfCol[listOfVer[i]] = listOfCol[i]
        listOfCol = []
        for i in dictOfCol:
            listOfCol.append(dictOfCol[i])
        clustCol["ve" + str(numOfVer) + "cl" + str(verClustCount[numOfVer])] =\
            listOfCol

complexCount = 1
ofh = open(args.output_file[0], "w")

if args.l is False:
    ofh.write("cluster,set,vertices,colours,subcluster,sc_set,sc_vertices," +
              "sc_colours\n")
else:
    ofh.write("cluster,list,vertices,colours,subcluster,sc_list," +
              "sc_vertices,sc_colours\n")

for i in clustCol:
    for j in clustCol:
        if args.l is False:
            if clustCol[j].issubset(clustCol[i]):
                ofh.write(i + ",\"" + str(list(clustCol[i])) + "\"," +
                          i.split("cl")[0][2:] + "," + str(len(clustCol[i])) +
                          "," + j + ",\"" + str(list(clustCol[j])) + "\"," +
                          j.split("cl")[0][2:] + "," + str(len(clustCol[j])) +
                          "\n")
        else:
            possibleSublist = clustCol[j]
            for k in clustCol[i]:
                if k in possibleSublist:
                    possibleSublist.remove(k)
            if len(possibleSublist) == 0:
                ofh.write(i + ",\"" + str(clustCol[i]) + "\"," +
                          i.split("cl")[0][2:] + "," +
                          str(len(set(clustCol[i]))) + "," + j + ",\"" +
                          str(clustCol[j]) + "\"," + j.split("cl")[0][2:] +
                          "," + str(len(set(clustCol[j]))) + "\n")
    logging.info("%s complexes subgraphed out of 36337", complexCount)
    complexCount += 1
ofh.close()
