import os
import sys
import copy
import time
import getopt
import logging
import warnings
import argparse
logging.basicConfig(level=logging.INFO)


class Graph(object):

    def __init__(self, graph_dict=None):
        '''Initialize graph with dictionaries for graph's representation and its
        information.'''
        if graph_dict is None:
            graph_dict = {}
        self.__graph_dict = graph_dict
        self.__information_dict = {}

    def __str__(self):
        '''Print graph's representational dictionary.'''
        return "Graph(%s) " % (self.__graph_dict)

    def __repr__(self):
        '''Print graph's representational dictionary and its ID.'''
        return "Graph(%s, %s) " % (self.__graph_dict, id(self))

    def __eq__(self, other):
        '''Return True if graphs' dictonaries are the same and False
        otherwise.'''
        return self.__graph_dict == other.__graph_dict

    def __ne__(self, other):
        '''Return True if graphs' dictonaries are not the same and False
        otherwise.'''
        return self.__graph_dict != other.__graph_dict

    def set_graph_dict(self, graph_dict):
        '''Setter for graph's representational dictionary.'''
        self.__graph_dict = graph_dict

    def set_information_dict(self, key, value):
        '''Setter for graph's informational dictionary.'''
        self.__information_dict[key] = value

    def get_graph_dict(self):
        '''Getter for graph's representational dictionary.'''
        return self.__graph_dict

    def get_information_dict(self):
        '''Getter for graph's informational dictionary.'''
        return self.__information_dict

    def __generate_edges(self):
        '''Return list of all edges in a graph.'''
        edges = []
        for vertex in self.__graph_dict:
            for neighbour in self.__graph_dict[vertex]:
                if {vertex, neighbour} not in edges:
                    edges.append({vertex, neighbour})
        return edges

    def edges(self):
        '''Uses generate edges method. I will delete this ("edges") method after
        full implementation of algorithm.'''
        return self.__generate_edges()

    def vertices(self):
        '''Return list of all vertices in a graph.'''
        return list(self.__graph_dict.keys())

    def add_edge(self, vertex1, vertex2, weight, colour):
        '''Update graph's representational dictionary with a new edge's
        information.'''
        self.__graph_dict.update({vertex1: {vertex2: [weight, colour]}})

    def add_vertex(self, vertex):
        '''Update graph's representational dictionary with a new vertex.'''
        if vertex not in self.__graph_dict:
            self.__graph_dict[vertex] = {}

    def delete_edge(self, edge):
        '''Update graph's representational dictionary without one if the edge's
        information.'''
        edge = set(edge)
        (vertex1, vertex2) = tuple(edge)
        del self.__graph_dict[vertex1][vertex2]
        del self.__graph_dict[vertex2][vertex1]

    def get_ascending_array(self):
        '''Return a list of all edges in a graph in an ascending order. Used by
        algorithmical methods.'''
        gdict = dict(copy.deepcopy(self.__graph_dict))
        gdict_list = []
        for key in gdict:
            for key2 in gdict[key]:
                gdict_list.append([key, key2, gdict[key][key2][0],
                                  gdict[key][key2][1]])
                try:
                    del gdict[key2][key]
                except:
                    pass
        gdict_list.sort(key=lambda ev: ev[2])
        del gdict
        return gdict_list

    def is_connected(self, vertices_encountered=None, start_vertex=None):
        '''Return True if a graph is connected and False otherwise.'''
        if vertices_encountered is None:
            vertices_encountered = set()
        gdict = self.__graph_dict
        vertices = list(gdict.keys())
        if not start_vertex:
            start_vertex = vertices[0]
        vertices_encountered.add(start_vertex)
        if len(vertices_encountered) != len(vertices):
            for vertex in gdict[start_vertex]:
                if vertex not in vertices_encountered:
                    if self.is_connected(vertices_encountered, vertex):
                        return True
        else:
            return True
        return False

    def reverse_edges(self):
        '''Return graph with reciprocal edges.'''
        F = dict(copy.deepcopy(self.__graph_dict))
        for vertex in self.__graph_dict:
            for neighbour in self.__graph_dict[vertex]:
                self.__graph_dict[vertex][neighbour][0] = \
                            float(1)/self.__graph_dict[vertex][neighbour][0]
        H = dict(copy.deepcopy(self.__graph_dict))
        self.__graph_dict = dict(copy.deepcopy(F))
        return Graph(H)

    def dijkstra(self, start, end):
        '''Return shortest distance between two nodes.'''
        parents = {}
        nodes = tuple(self.__graph_dict.keys())
        distances = dict(copy.deepcopy(self.__graph_dict))
        visited = {}
        unvisited = {node: None for node in nodes}
        current = start
        currentDistance = 0
        unvisited[current] = currentDistance
        parents[current] = None
        while True:
            for neighbour, distance in distances[current].items():
                if neighbour not in unvisited:
                    continue
                newDistance = currentDistance + distance[0]
                if unvisited[neighbour] is None\
                        or unvisited[neighbour] > newDistance:
                    unvisited[neighbour] = newDistance
                    parents[neighbour] = current
            visited[current] = currentDistance
            del unvisited[current]
            if not unvisited:
                break
            candidates = [node for node in unvisited.items() if node[1]]
            try:
                current, currentDistance = sorted(candidates,
                                                  key=lambda x: x[1])[0]
            except:
                return -sys.maxint - 1
        return visited[end]

    def find_S2(self, start, end):
        '''Return minimal distance between the two nodes but a path is not
        longer than two edges long. Return +infinity if a path does not
        exist.'''
        min_distance = sys.maxint
        for transition in self.__graph_dict[start]:
            for end_of_transition in self.__graph_dict[transition]:
                distance = self.__graph_dict[start][transition][0]
                + self.__graph_dict[transition][end_of_transition][0]
                if (end_of_transition == end) and (min_distance > distance):
                    min_distance = distance
        if min_distance == sys.maxint:
            min_distance = -sys.maxint - 1
        return min_distance

    def connectivity(self):
        '''Return connectivity of a graph.'''
        def all_pairs(source):
            result = []
            for p1 in range(len(source)):
                for p2 in range(p1+1, len(source)):
                    result.append([source[p1], source[p2]])
            return result
        pairings = all_pairs(self.__graph_dict.keys())
        sum = 0
        for pair in pairings:
            sum += float(1)/self.dijkstra(pair[0], pair[1])
        return (float(sum)*2)/(len(pairings)*(len(pairings)-1)) * 2

    def naive_approach(self, gama):
        '''Return graph after NA algorithm implementation.'''
        F = dict(copy.deepcopy(self.__graph_dict))
        gdict_list = self.get_ascending_array()
        n = int(round(gama*(len(gdict_list) - (len(self.__graph_dict) - 1))))
        i = 1
        j = 0
        while i <= n:
            a = gdict_list[j][0]
            b = gdict_list[j][1]
            c = gdict_list[j][2]
            d = gdict_list[j][3]
            del(self.__graph_dict[a][b])
            del(self.__graph_dict[b][a])
            if self.is_connected():
                i += 1
            else:
                self.__graph_dict[a][b] = [c, d]
                self.__graph_dict[b][a] = [c, d]
            j += 1
        H = dict(copy.deepcopy(self.__graph_dict))
        self.__graph_dict = dict(copy.deepcopy(F))
        return Graph(H)

    def brute_force_approach(self, gama):
        '''Return graph after BF algorithm implementation.'''
        F = dict(copy.deepcopy(self.__graph_dict))
        n = int(round(gama*(len(self.get_ascending_array()) - (len(F) - 1))))
        M = []
        for r in range(1, n+1):
            edges = self.__generate_edges()
            rk_largest = -sys.maxint - 1
            e_largest = None
            for edge in edges:
                if M.count(edge) == 1:
                    continue
                edge = list(edge)
                del(self.__graph_dict[edge[1]][edge[0]])
                del(self.__graph_dict[edge[0]][edge[1]])
                if self.is_connected():
                    numerator = self.connectivity()
                    self.__graph_dict[edge[0]][edge[1]] = F[edge[0]][edge[1]]
                    self.__graph_dict[edge[1]][edge[0]] = F[edge[1]][edge[0]]
                    denumerator = self.connectivity()
                    rk = float(numerator)/denumerator
                    if rk > rk_largest:
                        rk_largest = rk
                        e_largest = edge
                else:
                    self.__graph_dict[edge[0]][edge[1]] = F[edge[0]][edge[1]]
                    self.__graph_dict[edge[1]][edge[0]] = F[edge[1]][edge[0]]
                    edges = self.__generate_edges()
                    M.append({edge[0], edge[1]})
            del(self.__graph_dict[e_largest[1]][e_largest[0]])
            del(self.__graph_dict[e_largest[0]][e_largest[1]])
        edges = self.__generate_edges()
        H = dict(copy.deepcopy(self.__graph_dict))
        self.__graph_dict = dict(copy.deepcopy(F))
        return Graph(H)

    def path_simplification(self, gama):
        '''Return graph after PS algorithm implementation.'''
        F = dict(copy.deepcopy(self.__graph_dict))
        n = int(round(gama*(len(self.get_ascending_array()) - (len(F) - 1))))
        M = []
        for r in range(1, n+1):
            edges = self.__generate_edges()
            k_largest = -sys.maxint - 1
            e_largest = None
            for edge in edges:
                if M.count(edge) == 1:
                    continue
                edge = list(edge)
                qe = self.__graph_dict[edge[1]][edge[0]][0]
                del(self.__graph_dict[edge[1]][edge[0]])
                del(self.__graph_dict[edge[0]][edge[1]])
                edges = self.__generate_edges()
                qS = self.dijkstra(edge[0], edge[1])
                if qS >= qe:
                    k = 1
                    e_largest = edge
                    self.__graph_dict[edge[0]][edge[1]] = F[edge[0]][edge[1]]
                    self.__graph_dict[edge[1]][edge[0]] = F[edge[1]][edge[0]]
                    break
                elif (0 < qS) and (qS < qe):
                    self.__graph_dict[edge[0]][edge[1]] = F[edge[0]][edge[1]]
                    self.__graph_dict[edge[1]][edge[0]] = F[edge[1]][edge[0]]
                    k = float(qS)/qe
                else:
                    self.__graph_dict[edge[0]][edge[1]] = F[edge[0]][edge[1]]
                    self.__graph_dict[edge[1]][edge[0]] = F[edge[1]][edge[0]]
                    k = -sys.maxint - 1
                    M.append({edge[0], edge[1]})
                if k > k_largest:
                    k_largest = k
                    e_largest = edge
            edges = self.__generate_edges()
            del(self.__graph_dict[e_largest[1]][e_largest[0]])
            del(self.__graph_dict[e_largest[0]][e_largest[1]])
        edges = self.__generate_edges()
        H = dict(copy.deepcopy(self.__graph_dict))
        self.__graph_dict = dict(copy.deepcopy(F))
        return Graph(H)

    def combinational_approach(self, gama):
        '''Return graph after CB algorithm implementation.'''
        F = dict(copy.deepcopy(self.__graph_dict))
        n = int(round(gama*(len(self.get_ascending_array()) - (len(F) - 1))))
        r = 1
        find = True
        while r <= n and find is True:
            edges = self.__generate_edges()
            k_largest = -sys.maxint - 1
            e_largest = None
            for edge in edges:
                edge = list(edge)
                qe = self.__graph_dict[edge[1]][edge[0]][0]
                del(self.__graph_dict[edge[1]][edge[0]])
                del(self.__graph_dict[edge[0]][edge[1]])
                qS2e = self.find_S2(edge[0], edge[1])
                if qS2e >= qe:
                    self.__graph_dict[edge[0]][edge[1]] = F[edge[0]][edge[1]]
                    self.__graph_dict[edge[1]][edge[0]] = F[edge[1]][edge[0]]
                    k = 1
                    k_largest = 1
                    e_largest = edge
                    break
                elif (0 < qS2e) and (qS2e < qe):
                    self.__graph_dict[edge[0]][edge[1]] = F[edge[0]][edge[1]]
                    self.__graph_dict[edge[1]][edge[0]] = F[edge[1]][edge[0]]
                    k = float(qS2e)/qe
                else:
                    self.__graph_dict[edge[0]][edge[1]] = F[edge[0]][edge[1]]
                    self.__graph_dict[edge[1]][edge[0]] = F[edge[1]][edge[0]]
                    k = -sys.maxint - 1
                if k > k_largest:
                    k_largest = k
                    e_largest = edge
            if k_largest > 0:
                del(self.__graph_dict[edge[1]][edge[0]])
                del(self.__graph_dict[edge[0]][edge[1]])
                r += 1
            else:
                find = False
        # Use Path Simplification algorithm if not all edges are pruned.
        if r <= n:
            M = []
            beginningIndex = r
            r = None
            for r in range(beginningIndex, n+1):
                edges = self.__generate_edges()
                k_largest = -sys.maxint - 1
                e_largest = None
                for edge in edges:
                    if M.count(edge) == 1:
                        continue
                    edge = list(edge)
                    qe = self.__graph_dict[edge[1]][edge[0]][0]
                    del(self.__graph_dict[edge[1]][edge[0]])
                    del(self.__graph_dict[edge[0]][edge[1]])
                    edges = self.__generate_edges()
                    qS = self.dijkstra(edge[0], edge[1])
                    if qS >= qe:
                        k = 1
                        e_largest = edge
                        self.__graph_dict[edge[0]][edge[1]] = \
                            F[edge[0]][edge[1]]
                        self.__graph_dict[edge[1]][edge[0]] = \
                            F[edge[1]][edge[0]]
                        break
                    elif (0 < qS) and (qS < qe):
                        self.__graph_dict[edge[0]][edge[1]] = \
                            F[edge[0]][edge[1]]
                        self.__graph_dict[edge[1]][edge[0]] = \
                            F[edge[1]][edge[0]]
                        k = float(qS)/qe
                    else:
                        self.__graph_dict[edge[0]][edge[1]] = \
                            F[edge[0]][edge[1]]
                        self.__graph_dict[edge[1]][edge[0]] = \
                            F[edge[1]][edge[0]]
                        k = -sys.maxint - 1
                        M.append({edge[0], edge[1]})
                    if k > k_largest:
                        k_largest = k
                        e_largest = edge
                edges = self.__generate_edges()
                del(self.__graph_dict[e_largest[1]][e_largest[0]])
                del(self.__graph_dict[e_largest[0]][e_largest[1]])
            edges = self.__generate_edges()
            H = dict(copy.deepcopy(self.__graph_dict))
            self.__graph_dict = dict(copy.deepcopy(F))
            return Graph(H)
        H = dict(copy.deepcopy(self.__graph_dict))
        self.__graph_dict = dict(copy.deepcopy(F))
        return Graph(H)

    def naive_approach_colours(self, gama):
        '''Return graph after NA algorithm for colours implementation.'''
        F = dict(copy.deepcopy(self.__graph_dict))
        gdict_list = self.get_ascending_array()
        n = int(round(gama*(len(gdict_list) - (len(self.__graph_dict) - 1))))
        if n > 0:
            gdict_list.sort(key=lambda ev: (ev[2],ev[3]))
            uncut_colour = gdict_list[n - 1][3]
            if(gdict_list[n][3] == uncut_colour):
                while n > 0 and uncut_colour == gdict_list[n - 1][3]:
                    n = n - 1
        i = 1
        j = 0
        while i <= n:
            a = gdict_list[j][0]
            b = gdict_list[j][1]
            c = gdict_list[j][2]
            d = gdict_list[j][3]
            del(self.__graph_dict[a][b])
            del(self.__graph_dict[b][a])
            if self.is_connected():
                i += 1
            else:
                self.__graph_dict[a][b] = [c, d]
                self.__graph_dict[b][a] = [c, d]
            j += 1
        H = dict(copy.deepcopy(self.__graph_dict))
        self.__graph_dict = dict(copy.deepcopy(F))
        return Graph(H)

    def number_of_colours(self):
        '''Return number of edges' colours in a graph.'''
        different_colours = []
        for vertex in self.__graph_dict:
            for neighbour in self.__graph_dict[vertex]:
                if self.__graph_dict[vertex][neighbour][1] in \
                                                            different_colours:
                    pass
                else:
                    different_colours.append(
                        self.__graph_dict[vertex][neighbour][1])
        return len(different_colours)

    def get_pruned_elements(self, all_ascending, pruned_ascending):
        '''Return all pruned elements.'''
        first_set = set(map(tuple, all_ascending))
        secnd_set = set(map(tuple, pruned_ascending))
        pruned_edges = list(first_set.symmetric_difference(secnd_set))
        pruned_edges.sort(key=lambda ev: ev[2])
        return pruned_edges

    def read_file(self, file_name):
        '''Form graph's representational dictionary from a reading file.'''
        fh = open(file_name, "r")
        fh.readline()
        for line in fh:
            line = tuple(line.rstrip('\n').split())
            node_unique_description1 = "+".join([line[2], line[3]])
            node_unique_description2 = "+".join([line[5], line[6]])
            try:
                nud1 = node_unique_description1
                nud2 = node_unique_description2
                self.__graph_dict[nud1][nud2] = [float(line[10]), int(line[9])]
            except:
                self.__graph_dict[node_unique_description1] =\
                    {node_unique_description2: [float(line[10]), int(line[9])]}
            try:
                nud1 = node_unique_description1
                nud2 = node_unique_description2
                self.__graph_dict[nud2][nud1] \
                    = [float(line[10]), int(line[9])]
            except:
                self.__graph_dict[node_unique_description2] = \
                    {node_unique_description1: [float(line[10]), int(line[9])]}
        return None

    def write_file(self, file_name):
        '''Write graph's representational dictionary to a file.'''
        f = open(file_name, "w")
        graph_dictionary = self.get_graph_dict()
        f.write("d1+d1_symmetry    d2+d2_symmetry    interface_id    area\n")
        edges = []
        for vertex in graph_dictionary:
            for neighbour in graph_dictionary[vertex]:
                if {vertex, neighbour} not in edges:
                    f.write(vertex + "\t" + neighbour + "\t" +
                            str(graph_dictionary[vertex][neighbour][1]) +
                            "\t" +
                            str(graph_dictionary[vertex][neighbour][0]) + "\n")
                    edges.append({vertex, neighbour})
        f.close()

    def write_csv(self, file_name, algorithm):
        '''Write graph's informational dictionary to a file.'''

        try:
            nameOfcsvFile = args.cocsv[0]
        except:
            nameOfcsvFile = file_name.split('/', 1)[-1]+".csv"

        if os.path.exists(nameOfcsvFile):
            f = open(nameOfcsvFile, 'a')
        else:
            f = open(nameOfcsvFile, 'w')
            f.write("Graph,Algorithm,Gama,Running_time,rk," +
                    "Number_of_vertices,Number_of_edges_before," +
                    "Number_of_edges_after,Quantity_of_colours_before," +
                    "Quantity_of_colours_after," +
                    "Smallest_edge_of_initial_graph," +
                    "Second_smallest_edge_of_initial_graph," +
                    "Smallest_edge_of_processed_graph," +
                    "Second_smallest_edge_of_processed_graph," +
                    "Minimal_pruned_edge,Second_minimal_pruned_edge," +
                    "Biggest_edge_of_initial_graph," +
                    "Second_biggest_edge_of_initial_graph," +
                    "Biggest_edge_of_processed_graph," +
                    "Second_biggest_edge_of_processed_graph," +
                    "Biggest_pruned_edge,Second_biggest_pruned_edge")
        graph_dictionary = self.get_information_dict()
        f.write("\n")
        file_name = file_name.split('/', 1)[-1]
        f.write(file_name + "," + algorithm + "," +
                str(self.get_information_dict()["Gama"]) + "," +
                str(self.get_information_dict()["Running_time"]) + "," +
                str(self.get_information_dict()["rk"]) + "," +
                str(self.get_information_dict()["Number_of_vertices"]) + "," +
                str(self.get_information_dict()
                    ["Number_of_edges_before"]) + "," +
                str(self.get_information_dict()
                    ["Number_of_edges_after"]) + "," +
                str(self.get_information_dict()
                    ["Quantity_of_colours_before"]) +
                "," +
                str(self.get_information_dict()["Quantity_of_colours_after"]) +
                "," +
                str(self.get_information_dict()
                    ["Smallest_edge_of_initial_graph"]) +
                "," +
                str(self.get_information_dict()
                    ["Second_smallest_edge_of_initial_graph"]) +
                "," +
                str(self.get_information_dict()
                    ["Smallest_edge_of_processed_graph"]) +
                "," +
                str(self.get_information_dict()
                    ["Second_smallest_edge_of_processed_graph"]) +
                "," +
                str(self.get_information_dict()["Minimal_pruned_edge"]) +
                "," +
                str(self.get_information_dict()
                    ["Second_minimal_pruned_edge"]) +
                "," +
                str(self.get_information_dict()
                    ["Biggest_edge_of_initial_graph"]) +
                "," +
                str(self.get_information_dict()
                    ["Second_biggest_edge_of_initial_graph"]) +
                "," +
                str(self.get_information_dict()
                    ["Biggest_edge_of_processed_graph"]) +
                "," +
                str(self.get_information_dict()
                    ["Second_biggest_edge_of_processed_graph"]) +
                "," +
                str(self.get_information_dict()["Biggest_pruned_edge"]) +
                "," +
                str(self.get_information_dict()["Second_biggest_pruned_edge"]))
        f.close()
        return None

    def pruning(self, file, algorithm, gama):
        '''Execute specified algorithm for both of the graph's dictionaries.'''
        graph.read_file(file)

        if not graph.is_connected():
            warningMessage = "Graph is not connected: " +\
                str(file) + "; " + str(algorithm) + "; " + str(gama)
            warnings.warn(warningMessage)
            return None

        try:
            if len(graph.vertices()) > args.l[0]:
                return None
        except:
            pass

        if len(graph.edges()) == 1:

            try:
                nameOfcsvFile = args.cocsv[0]
            except:
                nameOfcsvFile = file.split('/', 1)[-1]+".csv"

            if os.path.exists(nameOfcsvFile):
                f = open(nameOfcsvFile, 'a')
            else:
                f = open(nameOfcsvFile, 'w')
                f.write("Graph,Algorithm,Gama,Running_time,rk,"
                        "Number_of_vertices,Number_of_edges_before," +
                        "Number_of_edges_after,Quantity_of_colours_before," +
                        "Quantity_of_colours_after," +
                        "Smallest_edge_of_initial_graph," +
                        "Second_smallest_edge_of_initial_graph," +
                        "Smallest_edge_of_processed_graph," +
                        "Second_smallest_edge_of_processed_graph," +
                        "Minimal_pruned_edge,Second_minimal_pruned_edge," +
                        "Biggest_edge_of_initial_graph," +
                        "Second_biggest_edge_of_initial_graph," +
                        "Biggest_edge_of_processed_graph," +
                        "Second_biggest_edge_of_processed_graph," +
                        "Biggest_pruned_edge,Second_biggest_pruned_edge")
            f.write("\n")
            area = graph.get_ascending_array()[0][2]
            f.write(str(file.split('/', 1)[-1]) + "," + str(algorithm) + "," +
                    str(gama) + ",None,1," + str(len(self.vertices())) +
                    ",1,1,1,1," + str(area) + ",None," + str(area) +
                    ",None,None,None," + str(area) + ",None," + str(area) +
                    ",None,None,None")
            f.close()
            if args.wpf is True:
                graph.write_file(file + "_" + str(gama) +
                                 "_" + algorithm + "_Cytoscape.txt")
            return None

        if args.wpf is True:
            graph.write_file(file + "_Unchanged_Cytoscape.txt")

        Rgraph = graph.reverse_edges()

        Dgraph = None
        starting_time = None
        ending_time = None

        if algorithm == "NA":
            starting_time = time.time()
            Dgraph = graph.naive_approach(gama)
            ending_time = time.time()
        elif algorithm == "BF":
            starting_time = time.time()
            Dgraph = Rgraph.brute_force_approach(gama)
            ending_time = time.time()
            Dgraph = Dgraph.reverse_edges()
        elif algorithm == "PS":
            starting_time = time.time()
            Dgraph = Rgraph.path_simplification(gama)
            ending_time = time.time()
            Dgraph = Dgraph.reverse_edges()
        elif algorithm == "CB":
            starting_time = time.time()
            Dgraph = Rgraph.combinational_approach(gama)
            ending_time = time.time()
            Dgraph = Dgraph.reverse_edges()
        else:
            exit()

        if args.wpf is True:
            Dgraph.write_file(file + "_" + str(gama) + "_" +
                              algorithm + "_Cytoscape.txt")

        Dgraph.set_information_dict("Gama", gama)
        Dgraph.set_information_dict("Running_time",
                                    ending_time - starting_time)
        DRgraph = Dgraph.reverse_edges()
        Dgraph.set_information_dict("rk", DRgraph.connectivity() /
                                    Rgraph.connectivity())
        Dgraph.set_information_dict("Number_of_vertices",
                                    len(graph.vertices()))
        Dgraph.set_information_dict("Number_of_edges_before",
                                    len(graph.edges()))
        Dgraph.set_information_dict("Number_of_edges_after",
                                    len(Dgraph.edges()))
        Dgraph.set_information_dict("Quantity_of_colours_before",
                                    graph.number_of_colours())
        Dgraph.set_information_dict("Quantity_of_colours_after",
                                    Dgraph.number_of_colours())

        try:
            Dgraph.set_information_dict("Smallest_edge_of_initial_graph",
                                        graph.get_ascending_array()[0][2])
        except:
            Dgraph.set_information_dict("Smallest_edge_of_initial_graph", None)

        try:
            Dgraph.set_information_dict("Second_smallest_edge_of_" +
                                        "initial_graph",
                                        graph.get_ascending_array()[1][2])
        except:
            Dgraph.set_information_dict("Second_smallest_edge_of_" +
                                        "initial_graph",
                                        None)

        try:
            Dgraph.set_information_dict("Smallest_edge_of_processed_graph",
                                        Dgraph.get_ascending_array()[0][2])
        except:
            Dgraph.set_information_dict("Smallest_edge_of_processed_graph",
                                        None)

        try:
            Dgraph.set_information_dict("Second_smallest_edge_of_" +
                                        "processed_graph",
                                        Dgraph.get_ascending_array()[1][2])
        except:
            Dgraph.set_information_dict("Second_smallest_edge_of_" +
                                        "processed_graph",
                                        None)

        try:
            Dgraph.set_information_dict("Biggest_edge_of_initial_graph",
                                        graph.get_ascending_array()[-1][2])
        except:
            Dgraph.set_information_dict("Biggest_edge_of_initial_graph", None)

        try:
            Dgraph.set_information_dict("Second_biggest_edge_of_initial_graph",
                                        graph.get_ascending_array()[-2][2])
        except:
            Dgraph.set_information_dict("Second_biggest_edge_of_initial_graph",
                                        None)

        try:
            Dgraph.set_information_dict("Biggest_edge_of_processed_graph",
                                        Dgraph.get_ascending_array()[-1][2])
        except:
            Dgraph.set_information_dict("Biggest_edge_of_processed_graph",
                                        None)

        try:
            Dgraph.set_information_dict("Second_biggest_edge_of_processed_" +
                                        "graph",
                                        Dgraph.get_ascending_array()[-2][2])
        except:
            Dgraph.set_information_dict("Second_biggest_edge_of_processed_\
                graph", None)

        if len(graph.edges()) - len(Dgraph.edges()) > 1:
            Pruned = self.get_pruned_elements(graph.get_ascending_array(),
                                              Dgraph.get_ascending_array())
            Dgraph.set_information_dict("Minimal_pruned_edge", Pruned[0][2])
            Dgraph.set_information_dict("Second_minimal_pruned_edge",
                                        Pruned[1][2])
        elif len(graph.edges()) - len(Dgraph.edges()) == 1:
            Pruned = self.get_pruned_elements(graph.get_ascending_array(),
                                              Dgraph.get_ascending_array())
            Dgraph.set_information_dict("Minimal_pruned_edge", Pruned[0][2])
            Dgraph.set_information_dict("Second_minimal_pruned_edge", None)
        else:
            Dgraph.set_information_dict("Minimal_pruned_edge", None)
            Dgraph.set_information_dict("Second_minimal_pruned_edge", None)

        if len(graph.edges()) - len(Dgraph.edges()) > 1:
            Pruned = self.get_pruned_elements(graph.get_ascending_array(),
                                              Dgraph.get_ascending_array())
            Dgraph.set_information_dict("Biggest_pruned_edge", Pruned[-1][2])
            Dgraph.set_information_dict("Second_biggest_pruned_edge",
                                        Pruned[-2][2])
        elif len(graph.edges()) - len(Dgraph.edges()) == 1:
            Pruned = self.get_pruned_elements(graph.get_ascending_array(),
                                              Dgraph.get_ascending_array())
            Dgraph.set_information_dict("Biggest_pruned_edge", Pruned[0][2])
            Dgraph.set_information_dict("Second_biggest_pruned_edge", None)
        else:
            Dgraph.set_information_dict("Biggest_pruned_edge", None)
            Dgraph.set_information_dict("Second_biggest_pruned_edge", None)

        Dgraph.write_csv(file, algorithm)

        return None


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Parameters for graph(-s) ' +
                                     'pruning.')
    parser.add_argument('files', metavar='N', type=str, nargs='+',
                        help='file names with graphs within them.')
    parser.add_argument('-wpf', '--write_pruned_files', action='store_true',
                        dest='wpf', help='write pruned graphs to text files ' +
                        'for further analysis with Cytoscape.')
    parser.add_argument('-l', '--limit', type=int, nargs=1, metavar='', dest='l',
                        help='do not prune files with more vertices than ' +
                        'specified.')
    parser.add_argument('-cocsv', '--create_one_csv', nargs=1, type=str,
                        metavar='', dest='cocsv',
                        help='write all info about pruning only into one ' +
                        'specified csv file.')
    args = parser.parse_args()

    for parameter in args.files:
            graph = Graph()
            logging.info('Starting graph %s', parameter)

            for gamma10 in range(1, 11):
                gamma = gamma10 * 0.1
                logging.info('Trying %s gamma %s' % ("NA", gamma))
                graph.pruning(parameter, "NA", gamma) 

            for gamma10 in range(1, 11):
                gamma = gamma10 * 0.1
                logging.info('Trying %s gamma %s' % ("BF", gamma))
                graph.pruning(parameter, "BF", gamma)

            for gamma10 in range(1, 11):
                gamma = gamma10 * 0.1
                logging.info('Trying %s gamma %s' % ("PS", gamma))
                graph.pruning(parameter, "PS", gamma)

            for gamma10 in range(1, 11):
                gamma = gamma10 * 0.1
                logging.info('Trying %s gamma %s' % ("CB", gamma))
                graph.pruning(parameter, "CB", gamma)

