'''
Usage example:
python clustering.py protein_interactions_with_areas_no_domains

(\ used for folding lines)
head -n 3 protein_interactions_with_areas_no_domains:
pdb_id	biounit_no	d1	d1_symmetry	d1_cluster95	d2	\
d2_symmetry	d2_cluster95	interaction_id	interface_id	\
i_sequence_cluster_95	i_cluster_95_50	area
3vg8	1	3vg8_A	1	36249	3vg8_B	2	36249	726427	\
302731	37903	4	100.0001
3vg8	1	3vg8_A	2	36249	3vg8_B	1	36249	726428	\
302731	37903	4	100.0001

Output:
"Clusters%Y.%m.%d_%H:%M:%S"
'''

from datetime import datetime
import logging
import sys
import os
logging.basicConfig(level=logging.INFO)

# Gathering info to dictionaries
complexInfo = {}
complexVertCol = {}
with open(sys.argv[1], "r") as ifh:
    ifh.readline()
    edge = ifh.readline().split()
    while edge:
        complexID = str(edge[0]) + "_" + str(edge[1])
        vertex1 = str(edge[2]) + "_" + str(edge[3])
        colour1 = str(edge[4])
        vertex2 = str(edge[5]) + "_" + str(edge[6])
        colour2 = str(edge[7])
        if complexID in complexVertCol:
            complexVertCol[complexID][0].append(vertex1)
            complexVertCol[complexID][0].append(vertex2)
            complexVertCol[complexID][1].append(colour1)
            complexVertCol[complexID][1].append(colour2)
        else:
            complexVertCol[complexID] =\
                [[vertex1, vertex2], [colour1, colour2]]
        if complexID in complexInfo:
            complexInfo[complexID].append(edge)
        else:
            complexInfo[complexID] = [edge]
        edge = ifh.readline().split()
ifh.close()

# Changing complexVertCol dictionary values for clusters searching
for i in complexVertCol:
    complexVertCol[i][0] = len(set(complexVertCol[i][0]))
    complexVertCol[i][1] = set(complexVertCol[i][1])

# Creating new directory for storing clusters
pyFilePath = os.path.dirname(os.path.realpath(__file__))
dt = datetime.now().strftime("%Y.%m.%d_%H:%M:%S")
os.mkdir(pyFilePath + "/Clusters_" + dt)

# Creating directories according to clusters unique IDs (number of vertices
# and its number according to vertices) and writing files into it
complexCount = 1
verClustCount = {}
while complexVertCol:
    oneGroup = []
    complexesToDel = []
    numOfVer = None
    setOfCol = None
    for i in complexVertCol:
        if not oneGroup:
            oneGroup.append(i)
            numOfVer = complexVertCol[i][0]
            setOfCol = complexVertCol[i][1]
            complexesToDel.append(i)
            logging.info("%s complexes clustered out of 112396", complexCount)
            complexCount += 1
        else:
            if complexVertCol[i][0] == numOfVer and\
               complexVertCol[i][1] == setOfCol:
                oneGroup.append(i)
                complexesToDel.append(i)
                logging.info("%s complexes clustered out of 112396",
                             complexCount)
                complexCount += 1
    for i in complexesToDel:
        del complexVertCol[i]
    if not os.path.isdir(pyFilePath + "/Clusters_" + dt + "/" + "ver" +
                         str(numOfVer)):
        os.mkdir(pyFilePath + "/Clusters_" + dt + "/" + "ver" + str(numOfVer))
    if numOfVer in verClustCount:
        verClustCount[numOfVer] += 1
    else:
        verClustCount[numOfVer] = 1
    os.mkdir(pyFilePath + "/Clusters_" + dt + "/" + "ver" + str(numOfVer) +
             "/ve" + str(numOfVer) + "cl" + str(verClustCount[numOfVer]))
    for i in oneGroup:
        ofh = open(pyFilePath + "/Clusters_" + dt + "/" + "ver" +
                   str(numOfVer) + "/ve" + str(numOfVer) + "cl" +
                   str(verClustCount[numOfVer]) + "/" + i, "a")
        ofh.write("\t".join(map(str, ["pdb_id", "biounit_no", "d1",
                                      "d1_symmetry", "d1_cluster95", "d2",
                                      "d2_symmetry", "d2_cluster95",
                                      "interaction_id", "interface_id",
                                      "i_sequence_cluster_95",
                                      "i_cluster_95_50", "area"])) + "\n")
        for j in complexInfo[i]:
            ofh.write("\t".join(map(str, j)) + "\n")
        ofh.close()
