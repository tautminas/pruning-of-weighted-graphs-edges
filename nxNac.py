'''
example:
python nxNac.py 3ezz_1

head -3 3ezz_1 ("\" used for bending lines):
pdb_id    biounit_no    d1    d1_symmetry    d1_cluster95     d2    \
d2_symmetry    d2_cluster95    interaction_id     interface_id    area
3ezz    1    3ezz_C    3    31395    3ezz_F    5    31395    188038	\
77748	247.319426
3ezz	1	3ezz_C	1	31395	3ezz_F	8	31395	188039	\
77748	247.319426

Output:
3ezz_1.csv

P.S.
printGraph(G) method could be used for writing csv format info about graph's
edges into stdout (could be used for visualization with Cytoscape)
'''

import networkx as nx
import pandas as pd
import logging
import argparse
import time
import csv
import sys
import os
logging.basicConfig(level=logging.INFO)


parser = argparse.ArgumentParser(description='Parameters for graph(-s) ' +
                                  'pruning.')
parser.add_argument('input_file', metavar='N', type=str, nargs=1,
                     help='file names with graphs within them.')
parser.add_argument('-s', '--supplement', action='store_true', dest='s',
                     help='create new txt file with supplementary ' +
                     'attributes (complex_id, to_be_pruned, ' +
                     'betweenness_centrality)')
parser.add_argument('-d', '--disconnected', action='store_true', dest='d',
                     help='print graph\'s ID if it is disconnected')
parser.add_argument('-p', '--prune', action='store_true', dest='p',
                     help='create new csv file with pruning info of ' +
                     'all algorithms')
parser.add_argument('-o', '--onlyREBC', action='store_true', dest='o',
                     help='perform only REBC pruning')
args = parser.parse_args()


def readGraph(G, proteinComplex):
    with open(proteinComplex, "r") as ifh:
        ifh.readline()
        edge = ifh.readline().split()
        lastEdge = None
        while edge:
            # Adding first vertex, second vertex, weigth and colour as
            # edge parameters
            G.add_edge(str(edge[2])+str(edge[3]), str(edge[5])+str(edge[6]),
                       weight=float(edge[12]), colour=int(edge[9]))
            G.node[str(edge[2])+str(edge[3])]['cluster95'] = str(edge[4])
            G.node[str(edge[5])+str(edge[6])]['cluster95'] = str(edge[7])
            lastEdge = edge
            edge = ifh.readline().split()
    G.graph["complexID"] = str(lastEdge[0]) + "_" + str(lastEdge[1])
    ifh.close()


def printGraph(G):
    print("Node1,Node2,Colour,Weight")
    for edge in G.edges(data=True):
        print(str(edge[0]) + "," + str(edge[1]) + "," +
              str(edge[2]["colour"]) + "," + str(edge[2]["weight"]))


def reverseEdges(G):
    for edge in G.edges(data=True):
        edge[2]["weight"] = 1/float(edge[2]["weight"])


def getListPairs(sourceList):
    result = []
    for p1 in range(len(sourceList)):
        for p2 in range(p1+1, len(sourceList)):
            result.append([sourceList[p1], sourceList[p2]])
    return result


def getNumberOfColours(G):
    differentColours = []
    for edge in G.edges(data=True):
        if edge[2]["colour"] not in differentColours:
            differentColours.append(edge[2]["colour"])
    return len(differentColours)


def getConnectivity(G):
    reverseEdges(G)
    nodesPairs = getListPairs(list(G.nodes))
    conSum = 0
    for nodes in nodesPairs:
        conSum += 1/float(nx.dijkstra_path_length(G, nodes[0], nodes[1],
                                                  weight="weight"))
    reverseEdges(G)
    return conSum


def getWeightOfColour(colourId, edgesList):
    for edge in edgesList:
        if edge[2]["colour"] == colourId:
            return edge[2]["weight"]


def naiveApproachColours(G, gama):
    # Calculating number of edges to be pruned
    n = int(round(gama * (G.number_of_edges() - (G.number_of_nodes() - 1))))

    # Getting all the edges included to be pruned plus one for colour removal
    edgesToBeRemoved = []
    for v1, v2, data in sorted(G.edges(data=True),
                               key=lambda x:
                               (x[2]["weight"], x[2]["colour"]))[:n+1]:
        edgesToBeRemoved.append([v1, v2, data])

    # Removing partialy coloured edges from edgesToBeRemoved
    outOfBoundsColour = edgesToBeRemoved[-1][2]["colour"]
    for edge in edgesToBeRemoved[::-1]:
        if edge[2]["colour"] == outOfBoundsColour:
            edgesToBeRemoved.remove(edge)
        else:
            break

    # Getting all the colours included to be pruned
    coloursToBeRemoved = []
    for edge in edgesToBeRemoved:
        if edge[2]["colour"] not in coloursToBeRemoved:
            coloursToBeRemoved.append(edge[2]["colour"])

    # Pruning whole colour if after doing so graph is still connected
    for prunedColour in coloursToBeRemoved:
        prunedColourEdges = []
        for edge in G.edges(data=True):
            if edge[2]["colour"] == prunedColour:
                prunedColourEdges.append(edge)
        G.remove_edges_from(prunedColourEdges)
        if not nx.is_connected(G):
            G.add_edges_from(prunedColourEdges)


def naiveApproachColoursEdgesCentrality(G, gama):
    # Calculating number of edges to be pruned
    n = int(round(gama * (G.number_of_edges() - (G.number_of_nodes() - 1))))

    reverseEdges(G)
    centralityDict = nx.edge_betweenness_centrality(G, weight="weight")
    reverseEdges(G)

    sortedColours = []
    for w in sorted(centralityDict, key=centralityDict.get, reverse=False):
        sortedColours.append(G.get_edge_data(w[0], w[1])["colour"])
    # Getting rid of duplicates leaving only first occurrence
    uniqSortCol = list(pd.DataFrame(sortedColours, columns=[''])
                       .drop_duplicates(keep="first")[''])

    sortedEdgesByCentralityAndColour = []
    for colour in uniqSortCol:
        for edge in G.edges(data=True):
            if edge[2]["colour"] == colour:
                sortedEdgesByCentralityAndColour.append(edge)

    # Getting all the edges included to be pruned plus one for colour removal
    edgesToBeRemoved = []
    for v1, v2, data in sortedEdgesByCentralityAndColour[:n+1]:
        edgesToBeRemoved.append([v1, v2, data])

    # Removing partialy coloured edges from edgesToBeRemoved
    outOfBoundsColour = edgesToBeRemoved[-1][2]["colour"]
    for edge in edgesToBeRemoved[::-1]:
        if edge[2]["colour"] == outOfBoundsColour:
            edgesToBeRemoved.remove(edge)
        else:
            break

    # Getting all the colours included to be pruned
    coloursToBeRemoved = []
    for edge in edgesToBeRemoved:
        if edge[2]["colour"] not in coloursToBeRemoved:
            coloursToBeRemoved.append(edge[2]["colour"])

    # Pruning whole colour if after doing so graph is still connected
    for prunedColour in coloursToBeRemoved:
        prunedColourEdges = []
        for edge in G.edges(data=True):
            if edge[2]["colour"] == prunedColour:
                prunedColourEdges.append(edge)
        G.remove_edges_from(prunedColourEdges)
        if not nx.is_connected(G):
            G.add_edges_from(prunedColourEdges)


def naiveApproachColoursEdgesFlowCentrality(G, gama):
    # Calculating number of edges to be pruned
    n = int(round(gama * (G.number_of_edges() - (G.number_of_nodes() - 1))))

    reverseEdges(G)
    centralityDict = nx.edge_betweenness_centrality(G, weight="weight")
    reverseEdges(G)

    sortedColours = []
    for w in sorted(centralityDict, key=centralityDict.get, reverse=False):
        sortedColours.append(G.get_edge_data(w[0], w[1])["colour"])
    # Getting rid of duplicates leaving only first occurrence
    uniqSortCol = list(pd.DataFrame(sortedColours, columns=[''])
                       .drop_duplicates(keep="first")[''])

    sortedEdgesByCentralityAndColour = []
    for colour in uniqSortCol:
        for edge in G.edges(data=True):
            if edge[2]["colour"] == colour:
                sortedEdgesByCentralityAndColour.append(edge)

    # Getting all the edges included to be pruned plus one for colour removal
    edgesToBeRemoved = []
    for v1, v2, data in sortedEdgesByCentralityAndColour[:n+1]:
        edgesToBeRemoved.append([v1, v2, data])

    # Removing partialy coloured edges from edgesToBeRemoved
    outOfBoundsColour = edgesToBeRemoved[-1][2]["colour"]
    for edge in edgesToBeRemoved[::-1]:
        if edge[2]["colour"] == outOfBoundsColour:
            edgesToBeRemoved.remove(edge)
        else:
            break

    # Getting all the colours included to be pruned
    coloursToBeRemoved = []
    for edge in edgesToBeRemoved:
        if edge[2]["colour"] not in coloursToBeRemoved:
            coloursToBeRemoved.append(edge[2]["colour"])

    # Pruning whole colour if after doing so graph is still connected
    for prunedColour in coloursToBeRemoved:
        prunedColourEdges = []
        for edge in G.edges(data=True):
            if edge[2]["colour"] == prunedColour:
                prunedColourEdges.append(edge)
        G.remove_edges_from(prunedColourEdges)
        if not nx.is_connected(G):
            G.add_edges_from(prunedColourEdges)


def naiveMovingApproachColours(G, gama):
    # Calculating number of edges to be pruned
    n = int(round(gama * (G.number_of_edges() - (G.number_of_nodes() - 1))))

    # Getting list of all the colours sorted by weight and colour number
    sortedColours = []
    for v1, v2, data in sorted(G.edges(data=True),
                               key=lambda x:
                               (x[2]["weight"], x[2]["colour"])):
        sortedColours.append(data["colour"])

    # Getting list of all the unique values in sorted colours list
    uniqSortCol = []
    [uniqSortCol.append(i) for i in sortedColours if not uniqSortCol.count(i)]

    # Pruning whole colour if after doing so graph is still connected and there
    # are edges left to be pruned
    numOfPrunedEdges = 0
    for prunedColour in uniqSortCol:
        prunedColourEdges = []
        for edge in G.edges(data=True):
            if edge[2]["colour"] == prunedColour:
                prunedColourEdges.append(edge)
        G.remove_edges_from(prunedColourEdges)
        numOfPrunedEdges += len(prunedColourEdges)
        if not nx.is_connected(G) or numOfPrunedEdges > n:
            G.add_edges_from(prunedColourEdges)
            numOfPrunedEdges -= len(prunedColourEdges)
        if numOfPrunedEdges == n:
            break


def naiveMovingApproachColoursEdgesCentrality(G, gama):
    # Calculating number of edges to be pruned
    n = int(round(gama * (G.number_of_edges() - (G.number_of_nodes() - 1))))

    reverseEdges(G)
    centralityDict = nx.edge_betweenness_centrality(G, weight="weight")
    reverseEdges(G)

    sortedColours = []
    for w in sorted(centralityDict, key=centralityDict.get, reverse=False):
        sortedColours.append(G.get_edge_data(w[0], w[1])["colour"])
    # Getting rid of duplicates leaving only first occurrence
    uniqSortCol = list(pd.DataFrame(sortedColours, columns=[''])
                       .drop_duplicates(keep="first")[''])

    # Pruning whole colour if after doing so graph is still connected and there
    # are edges left to be pruned
    numOfPrunedEdges = 0
    for prunedColour in uniqSortCol:
        prunedColourEdges = []
        for edge in G.edges(data=True):
            if edge[2]["colour"] == prunedColour:
                prunedColourEdges.append(edge)
        G.remove_edges_from(prunedColourEdges)
        numOfPrunedEdges += len(prunedColourEdges)
        if not nx.is_connected(G) or numOfPrunedEdges > n:
            G.add_edges_from(prunedColourEdges)
            numOfPrunedEdges -= len(prunedColourEdges)
        if numOfPrunedEdges == n:
            break


def naiveMovingApproachColoursEdgesFlowCentrality(G, gama):
    # Calculating number of edges to be pruned
    n = int(round(gama * (G.number_of_edges() - (G.number_of_nodes() - 1))))

    reverseEdges(G)
    centralityDict = \
        nx.edge_current_flow_betweenness_centrality(G, weight="weight")
    reverseEdges(G)

    sortedColours = []
    for w in sorted(centralityDict, key=centralityDict.get, reverse=False):
        sortedColours.append(G.get_edge_data(w[0], w[1])["colour"])
    # Getting rid of duplicates leaving only first occurrence
    uniqSortCol = list(pd.DataFrame(sortedColours, columns=[''])
                       .drop_duplicates(keep="first")[''])

    # Pruning whole colour if after doing so graph is still connected and there
    # are edges left to be pruned
    numOfPrunedEdges = 0
    for prunedColour in uniqSortCol:
        prunedColourEdges = []
        for edge in G.edges(data=True):
            if edge[2]["colour"] == prunedColour:
                prunedColourEdges.append(edge)
        G.remove_edges_from(prunedColourEdges)
        numOfPrunedEdges += len(prunedColourEdges)
        if not nx.is_connected(G) or numOfPrunedEdges > n:
            G.add_edges_from(prunedColourEdges)
            numOfPrunedEdges -= len(prunedColourEdges)
        if numOfPrunedEdges == n:
            break


def bruteForceApproach(G, gama):
    # Calculating number of edges to be pruned
    n = int(round(gama * (G.number_of_edges() - (G.number_of_nodes() - 1))))

    # Getting a list of all the unique colours
    uniqColours = []
    for v1, v2, data in G.edges(data=True):
        if str(data["colour"]) not in uniqColours:
            uniqColours.append(str(data["colour"]))

    # Brute force approach algorithm
    numOfPrunedEdges = 0
    finished = False
    while not finished:
        # Searching for colour which removal would result in connected graph
        # best rk. Also number of cut edges is taken into account
        rkLargest = -sys.maxint - 1
        eLargest = None
        for currentColour in uniqColours:
            prunedColourEdges = []
            for edge in G.edges(data=True):
                if edge[2]["colour"] == int(currentColour):
                    prunedColourEdges.append(edge)
            rk = getConnectivity(G)
            G.remove_edges_from(prunedColourEdges)
            if nx.is_connected(G):
                numOfPrunedEdges += len(prunedColourEdges)
                rk = getConnectivity(G) / float(rk)
                if rk > rkLargest and numOfPrunedEdges <= n:
                    rkLargest = rk
                    eLargest = currentColour
                numOfPrunedEdges -= len(prunedColourEdges)
            G.add_edges_from(prunedColourEdges)
        # If such colour was found it is cut and searching is continued
        if eLargest is not None:
            prunedColourEdges = []
            for edge in G.edges(data=True):
                if edge[2]["colour"] == int(eLargest):
                    prunedColourEdges.append(edge)
            G.remove_edges_from(prunedColourEdges)
            numOfPrunedEdges += len(prunedColourEdges)
            uniqColours.remove(eLargest)
        # If no such colour was found algoritm stops
        else:
            finished = True


def naiveMovingAverageApproachColours(G, gama):
    # Calculating number of edges to be pruned
    n = int(round(gama * (G.number_of_edges() - (G.number_of_nodes() - 1))))

    # Calculating average of edges weights
    averageOfEdgWei = G.size(weight="weight") / G.number_of_edges()

    # Getting list of all the colours sorted by weight and colour number
    sortedColours = []
    for v1, v2, data in sorted(G.edges(data=True),
                               key=lambda x:
                               (x[2]["weight"], x[2]["colour"])):
        sortedColours.append(data["colour"])

    # Getting list of all the unique values in sorted colours list
    uniqSortCol = []
    [uniqSortCol.append(i) for i in sortedColours if not uniqSortCol.count(i)]

    # Pruning whole colour if after doing so graph is still connected and there
    # are edges left to be pruned and edge of colours is smaller than average
    numOfPrunedEdges = 0
    for prunedColour in uniqSortCol:
        prunedColourEdges = []
        for edge in G.edges(data=True):
            if edge[2]["colour"] == prunedColour:
                prunedColourEdges.append(edge)
        if prunedColourEdges[0][2]["weight"] > averageOfEdgWei:
            break
        G.remove_edges_from(prunedColourEdges)
        numOfPrunedEdges += len(prunedColourEdges)
        if not nx.is_connected(G) or numOfPrunedEdges > n:
            G.add_edges_from(prunedColourEdges)
            numOfPrunedEdges -= len(prunedColourEdges)
        if numOfPrunedEdges == n:
            break


def naiveMovingAverageApproachColoursEdgesCentrality(G, gama):
    # Calculating number of edges to be pruned
    n = int(round(gama * (G.number_of_edges() - (G.number_of_nodes() - 1))))

    reverseEdges(G)
    centralityDict = nx.edge_betweenness_centrality(G, weight="weight")
    reverseEdges(G)

    # Calculating average of edges (centrality) weights
    count = 0
    totalSum = 0
    for key in centralityDict:
        count += 1
        totalSum += centralityDict[key]
    averageOfEdgWei = totalSum/count

    sortedColours = []
    for w in sorted(centralityDict, key=centralityDict.get, reverse=False):
        sortedColours.append(G.get_edge_data(w[0], w[1])["colour"])
    # Getting rid of duplicates leaving only first occurrence
    uniqSortCol = list(pd.DataFrame(sortedColours, columns=[''])
                       .drop_duplicates(keep="first")[''])

    # Pruning whole colour if after doing so graph is still connected and there
    # are edges left to be pruned and edge of colours is smaller than average
    numOfPrunedEdges = 0
    for prunedColour in uniqSortCol:
        prunedColourEdges = []
        for edge in G.edges(data=True):
            if edge[2]["colour"] == prunedColour:
                prunedColourEdges.append(edge)
        if (prunedColourEdges[0][0],
                prunedColourEdges[0][1]) in centralityDict:
            prunedEdgeCentrality = centralityDict[(prunedColourEdges[0][0],
                                                   prunedColourEdges[0][1])]
        else:
            prunedEdgeCentrality = centralityDict[(prunedColourEdges[0][1],
                                                   prunedColourEdges[0][0])]
        if prunedEdgeCentrality > averageOfEdgWei:
            break
        G.remove_edges_from(prunedColourEdges)
        numOfPrunedEdges += len(prunedColourEdges)
        if not nx.is_connected(G) or numOfPrunedEdges > n:
            G.add_edges_from(prunedColourEdges)
            numOfPrunedEdges -= len(prunedColourEdges)
        if numOfPrunedEdges == n:
            break


def naiveMovingAverageApproachColoursEdgesFlowCentrality(G, gama):
    # Calculating number of edges to be pruned
    n = int(round(gama * (G.number_of_edges() - (G.number_of_nodes() - 1))))

    reverseEdges(G)
    centralityDict = \
        nx.edge_current_flow_betweenness_centrality(G, weight="weight")
    reverseEdges(G)

    # Calculating average of edges (centrality) weights
    count = 0
    totalSum = 0
    for key in centralityDict:
        count += 1
        totalSum += centralityDict[key]
    averageOfEdgWei = totalSum/count

    sortedColours = []
    for w in sorted(centralityDict, key=centralityDict.get, reverse=False):
        sortedColours.append(G.get_edge_data(w[0], w[1])["colour"])
    # Getting rid of duplicates leaving only first occurrence
    uniqSortCol = list(pd.DataFrame(sortedColours, columns=[''])
                       .drop_duplicates(keep="first")[''])

    # Pruning whole colour if after doing so graph is still connected and there
    # are edges left to be pruned and edge of colours is smaller than average
    numOfPrunedEdges = 0
    for prunedColour in uniqSortCol:
        prunedColourEdges = []
        for edge in G.edges(data=True):
            if edge[2]["colour"] == prunedColour:
                prunedColourEdges.append(edge)
        if (prunedColourEdges[0][0],
                prunedColourEdges[0][1]) in centralityDict:
            prunedEdgeCentrality = centralityDict[(prunedColourEdges[0][0],
                                                   prunedColourEdges[0][1])]
        else:
            prunedEdgeCentrality = centralityDict[(prunedColourEdges[0][1],
                                                   prunedColourEdges[0][0])]
        if prunedEdgeCentrality > averageOfEdgWei:
            break
        G.remove_edges_from(prunedColourEdges)
        numOfPrunedEdges += len(prunedColourEdges)
        if not nx.is_connected(G) or numOfPrunedEdges > n:
            G.add_edges_from(prunedColourEdges)
            numOfPrunedEdges -= len(prunedColourEdges)
        if numOfPrunedEdges == n:
            break


def naiveLowerRk(G, lrk):
    # Getting list of all the colours sorted by weight and colour number
    sortedColours = []
    for v1, v2, data in sorted(G.edges(data=True),
                               key=lambda x:
                               (x[2]["weight"], x[2]["colour"])):
        sortedColours.append(data["colour"])

    # Getting list of all the unique values in sorted colours list
    uniqSortCol = []
    [uniqSortCol.append(i) for i in sortedColours if not uniqSortCol.count(i)]

    # Pruning whole colour if after doing so graph is still connected and has
    # larger or equal value to upper bound of rk
    initRk = getConnectivity(G)
    for prunedColour in uniqSortCol:
        prunedColourEdges = []
        for edge in G.edges(data=True):
            if edge[2]["colour"] == prunedColour:
                prunedColourEdges.append(edge)
        G.remove_edges_from(prunedColourEdges)
        if nx.is_connected(G):
            rk = getConnectivity(G) / float(initRk)
            if rk < lrk:
                G.add_edges_from(prunedColourEdges)
        else:
            G.add_edges_from(prunedColourEdges)


def naiveLowerROfSumsk(G, lrOfSumsk):
    # Getting list of all the colours sorted by weight and colour number
    sortedColours = []
    for v1, v2, data in sorted(G.edges(data=True),
                               key=lambda x:
                               (x[2]["weight"], x[2]["colour"])):
        sortedColours.append(data["colour"])

    # Getting list of all the unique values in sorted colours list
    uniqSortCol = []
    [uniqSortCol.append(i) for i in sortedColours if not uniqSortCol.count(i)]

    # Pruning whole colour if after doing so graph is still connected and has
    # larger or equal value to upper bound of rk
    initROfSumsk = G.size(weight="weight")
    for prunedColour in uniqSortCol:
        prunedColourEdges = []
        for edge in G.edges(data=True):
            if edge[2]["colour"] == prunedColour:
                prunedColourEdges.append(edge)
        G.remove_edges_from(prunedColourEdges)
        if nx.is_connected(G):
            rOfSumsk = G.size(weight="weight") / float(initROfSumsk)
            if rOfSumsk < lrOfSumsk:
                G.add_edges_from(prunedColourEdges)
        else:
            G.add_edges_from(prunedColourEdges)


def naiveLowerFirstRk(G, lrk):
    # Getting list of all the colours sorted by weight and colour number
    sortedColours = []
    for v1, v2, data in sorted(G.edges(data=True),
                               key=lambda x:
                               (x[2]["weight"], x[2]["colour"])):
        sortedColours.append(data["colour"])

    # Getting list of all the unique values in sorted colours list
    uniqSortCol = []
    [uniqSortCol.append(i) for i in sortedColours if not uniqSortCol.count(i)]

    # Pruning whole colour if after doing so graph is still connected and has
    # larger or equal value to upper bound of rk
    initRk = getConnectivity(G)
    for prunedColour in uniqSortCol:
        prunedColourEdges = []
        for edge in G.edges(data=True):
            if edge[2]["colour"] == prunedColour:
                prunedColourEdges.append(edge)
        G.remove_edges_from(prunedColourEdges)
        if nx.is_connected(G):
            rk = getConnectivity(G) / float(initRk)
            if rk < lrk:
                G.add_edges_from(prunedColourEdges)
                break
        else:
            G.add_edges_from(prunedColourEdges)
            break


def naiveLowerFirstROfSumsk(G, lrOfSumsk):
    # Getting list of all the colours sorted by weight and colour number
    sortedColours = []
    for v1, v2, data in sorted(G.edges(data=True),
                               key=lambda x:
                               (x[2]["weight"], x[2]["colour"])):
        sortedColours.append(data["colour"])

    # Getting list of all the unique values in sorted colours list
    uniqSortCol = []
    [uniqSortCol.append(i) for i in sortedColours if not uniqSortCol.count(i)]

    # Pruning whole colour if after doing so graph is still connected and has
    # larger or equal value to upper bound of rk
    initROfSumsk = G.size(weight="weight")
    for prunedColour in uniqSortCol:
        prunedColourEdges = []
        for edge in G.edges(data=True):
            if edge[2]["colour"] == prunedColour:
                prunedColourEdges.append(edge)
        G.remove_edges_from(prunedColourEdges)
        if nx.is_connected(G):
            rOfSumsk = G.size(weight="weight") / float(initROfSumsk)
            if rOfSumsk < lrOfSumsk:
                G.add_edges_from(prunedColourEdges)
                break
        else:
            G.add_edges_from(prunedColourEdges)
            break


def redundantEdgeBetweennessCentrality(G, unnecessary):
    reverseEdges(G)
    centralityDict = nx.edge_betweenness_centrality(G, weight="weight")
    reverseEdges(G)
    nx.set_edge_attributes(G, centralityDict, "betweenness")
    firstColourInSort = None
    edgesForPruning = []
    edgesForPruningCentrality = []
    for v1, v2, data in sorted(G.edges(data=True),
                               key=lambda x: (x[2]["betweenness"],
                               x[2]["weight"], x[2]["colour"])):
        # If new colour is reached:
        if firstColourInSort != data["colour"]:
            # 1) Old colour's edges are removed if all betweennesses are zeroes
            isAllCentralitiesZeroes = True
            for i in edgesForPruningCentrality:
                if i != 0:
                    isAllCentralitiesZeroes = False
            if isAllCentralitiesZeroes is True and \
               len(edgesForPruningCentrality) > 0:
                G.remove_edges_from(edgesForPruning)
            firstColourInSort = data["colour"]
            # 2) Data is collected if betweenness is equal to 0
            if data["betweenness"] == 0:
                edgesForPruning = [[v1, v2]]
                edgesForPruningCentrality = [data["betweenness"]]
            # 3) Algorithm is stopped because there are no more colours with
            #    all betweenness values equal to 0
            else:
                break
        # Otherwise edges data is collected until other new colour is reached.
        else:
            edgesForPruning.append([v1, v2])
            edgesForPruningCentrality.append(data["betweenness"])


def redundantEdgeBetweennessCentrality500(G, unnecessary):
    reverseEdges(G)
    centralityDict = nx.edge_betweenness_centrality(G, weight="weight")
    reverseEdges(G)
    nx.set_edge_attributes(G, centralityDict, "betweenness")
    firstColourInSort = None
    edgesForPruning = []
    edgesForPruningCentrality = []
    for v1, v2, data in sorted(G.edges(data=True),
                               key=lambda x: (x[2]["betweenness"],
                               x[2]["weight"], x[2]["colour"])):
        # If new colour is reached:
        if firstColourInSort != data["colour"]:
            # 1) Old colour's edges are removed if all betweennesses are zeroes
            isAllCentralitiesZeroes = True
            for i in edgesForPruningCentrality:
                if i != 0:
                    isAllCentralitiesZeroes = False
            if isAllCentralitiesZeroes is True and \
               len(edgesForPruningCentrality) > 0:
                G.remove_edges_from(edgesForPruning)
            firstColourInSort = data["colour"]
            # 2) Data is collected if betweenness is equal to 0
            if data["betweenness"] == 0 and data["weight"] < 500:
                edgesForPruning = [[v1, v2]]
                edgesForPruningCentrality = [data["betweenness"]]
            # 3) Algorithm is stopped because there are no more colours with
            #    all betweenness values equal to 0
            else:
                break
        # Otherwise edges data is collected until other new colour is reached.
        else:
            edgesForPruning.append([v1, v2])
            edgesForPruningCentrality.append(data["betweenness"])


def writePruningInfo(G, algorithm, gama):
    readGraph(G, args.input_file[0])

    if G.number_of_edges() < 2 or not nx.is_connected(G):
        logging.info('Graph is not valid for %s pruning %s with %s gama value',
                     algorithm, args.input_file[0], gama)
        return 0

    # Opening csv file and adding header if it does not have one
    if os.path.exists(args.input_file[0] + ".csv"):
        ofh = open(args.input_file[0] + ".csv", "a")
        fieldnames = ["Graph", "Algorithm", "Gama", "Running_time", "rk",
                      "Number_of_vertices", "Number_of_edges_before",
                      "Number_of_edges_after", "Quantity_of_colours_before",
                      "Quantity_of_colours_after",
                      "Smallest_edge_of_initial_graph",
                      "Second_smallest_edge_of_initial_graph",
                      "Smallest_edge_of_processed_graph",
                      "Second_smallest_edge_of_processed_graph",
                      "Minimal_pruned_edge", "Second_minimal_pruned_edge",
                      "Biggest_edge_of_initial_graph",
                      "Second_biggest_edge_of_initial_graph",
                      "Biggest_edge_of_processed_graph",
                      "Second_biggest_edge_of_processed_graph",
                      "Biggest_pruned_edge", "Second_biggest_pruned_edge",
                      "rOfSumsk", "Minimal_pruned_colour_id",
                      "Minimal_pruned_colour",
                      "Second_minimal_pruned_colour_id",
                      "Second_minimal_pruned_colour",
                      "Biggest_pruned_colour_id", "Biggest_pruned_colour",
                      "Second_biggest_pruned_colour_id",
                      "Second_biggest_pruned_colour", "Is_homo",
                      "Has_peptide", "Area_sum_before", "Area_sum_after"]
        writer = csv.DictWriter(ofh, fieldnames=fieldnames)
    else:
        ofh = open(args.input_file[0] + ".csv", "w")
        fieldnames = ["Graph", "Algorithm", "Gama", "Running_time", "rk",
                      "Number_of_vertices", "Number_of_edges_before",
                      "Number_of_edges_after", "Quantity_of_colours_before",
                      "Quantity_of_colours_after",
                      "Smallest_edge_of_initial_graph",
                      "Second_smallest_edge_of_initial_graph",
                      "Smallest_edge_of_processed_graph",
                      "Second_smallest_edge_of_processed_graph",
                      "Minimal_pruned_edge", "Second_minimal_pruned_edge",
                      "Biggest_edge_of_initial_graph",
                      "Second_biggest_edge_of_initial_graph",
                      "Biggest_edge_of_processed_graph",
                      "Second_biggest_edge_of_processed_graph",
                      "Biggest_pruned_edge", "Second_biggest_pruned_edge",
                      "rOfSumsk", "Minimal_pruned_colour_id",
                      "Minimal_pruned_colour",
                      "Second_minimal_pruned_colour_id",
                      "Second_minimal_pruned_colour",
                      "Biggest_pruned_colour_id", "Biggest_pruned_colour",
                      "Second_biggest_pruned_colour_id",
                      "Second_biggest_pruned_colour", "Is_homo",
                      "Has_peptide", "Area_sum_before", "Area_sum_after"]
        writer = csv.DictWriter(ofh, fieldnames=fieldnames)
        writer.writeheader()

    # Data gathering before pruning
    logging.info('Gathering data before %s pruning %s with %s gama value',
                 algorithm, args.input_file[0], gama)
    # rk = getConnectivity(G)  # altering variable after pruning
    rOfSumsk = G.size(weight="weight")  # altering variable after pruning
    arSumBef = G.size(weight="weight")
    if len(set(list(nx.get_node_attributes(G,'cluster95').values()))) == 1:
        isHomo = 1
    else:
        isHomo = 0
    if "NULL" in set(list(nx.get_node_attributes(G,'cluster95').values())):
        hasPeptide = 1
    else:
        hasPeptide = 0
    numOfVer = len(list(G.nodes))
    numOfEdgBef = len(list(G.edges))
    numOfColBef = getNumberOfColours(G)
    sortedInitialEdges = sorted(G.edges(data=True),
                                key=lambda x: (x[2]["weight"], x[2]["colour"]))
    smalEdgOfInitG = sortedInitialEdges[0][2]["weight"]
    secSmalEdgOfInitG = sortedInitialEdges[1][2]["weight"]
    bigEdgOfInitG = sortedInitialEdges[-1][2]["weight"]
    secBigEdgOfInitG = sortedInitialEdges[-2][2]["weight"]

    # Pruning and calculating time
    logging.info('Executing %s pruning %s with %s gama value', algorithm,
                 args.input_file[0], gama)
    startingTime = time.time()

    switcher = {
        "NAC": naiveApproachColours,
        "NMAC": naiveMovingApproachColours,
        "BF": bruteForceApproach,
        "NMAvAC": naiveMovingAverageApproachColours,
        "NLRk": naiveLowerRk,
        "NLROSk": naiveLowerROfSumsk,
        "NLFRk": naiveLowerFirstRk,
        "NLFROSk": naiveLowerFirstROfSumsk,
        "NACEC": naiveApproachColoursEdgesCentrality,
        "NACEFC": naiveApproachColoursEdgesFlowCentrality,
        "NMACEC": naiveMovingApproachColoursEdgesCentrality,
        "NMACEFC": naiveMovingApproachColoursEdgesFlowCentrality,
        "NMAvACEC": naiveMovingAverageApproachColoursEdgesCentrality,
        "NMAvACEFC": naiveMovingAverageApproachColoursEdgesFlowCentrality,
        "REBC": redundantEdgeBetweennessCentrality,
        "REBC500": redundantEdgeBetweennessCentrality500,
    }
    pruningFunc = switcher.get(algorithm)  # Could supplement for default value
    pruningFunc(G, gama)

    endingTime = time.time()
    runningTime = endingTime - startingTime

    # Data gathering after pruning
    logging.info('Gathering data after %s pruning %s with %s gama value',
                 algorithm, args.input_file[0], gama)
    # rk = getConnectivity(G) / float(rk)  # alteration
    rOfSumsk = G.size(weight="weight") / float(rOfSumsk)  # alteration
    arSumAft = G.size(weight="weight")
    numOfEdgAft = len(list(G.edges))
    numOfColAft = getNumberOfColours(G)
    sortedProcessedGraphEdges = sorted(G.edges(data=True),
                                       key=lambda x: (x[2]["weight"],
                                                      x[2]["colour"]))
    smalEdgOfProcG = sortedProcessedGraphEdges[0][2]["weight"]
    secSmalEdgOfProcG = sortedProcessedGraphEdges[1][2]["weight"]
    bigEdgOfProcG = sortedProcessedGraphEdges[-1][2]["weight"]
    secBigEdgOfProcG = sortedProcessedGraphEdges[-2][2]["weight"]
    sortedPrunedEdges = sorted([prunedEdge for prunedEdge in sortedInitialEdges
                               if prunedEdge not in sortedProcessedGraphEdges],
                               key=lambda x: (x[2]["weight"], x[2]["colour"]))

    sortedPrunedColours = []
    for v1, v2, data in sorted(sortedPrunedEdges,
                               key=lambda x:
                               (x[2]["weight"], x[2]["colour"])):
        sortedPrunedColours.append(data["colour"])

    sortedUniqPrunedColours = []
    [sortedUniqPrunedColours.append(i) for i in sortedPrunedColours
        if not sortedUniqPrunedColours.count(i)]

    if len(sortedPrunedEdges) > 1:
        minPruEdg = sortedPrunedEdges[0][2]["weight"]
        secMinPruEdg = sortedPrunedEdges[1][2]["weight"]
        bigPruEdg = sortedPrunedEdges[-1][2]["weight"]
        secBigPruEdg = sortedPrunedEdges[-2][2]["weight"]
    elif len(sortedPrunedEdges) == 1:
        minPruEdg = sortedPrunedEdges[0][2]["weight"]
        secMinPruEdg = None
        bigPruEdg = sortedPrunedEdges[-1][2]["weight"]
        secBigPruEdg = None
    else:
        minPruEdg = None
        secMinPruEdg = None
        bigPruEdg = None
        secBigPruEdg = None

    if len(sortedUniqPrunedColours) > 1:
        minPruColId = sortedUniqPrunedColours[0]
        minPruCol = getWeightOfColour(minPruColId, sortedPrunedEdges)
        secMinPruColId = sortedUniqPrunedColours[1]
        secMinPruCol = getWeightOfColour(secMinPruColId, sortedPrunedEdges)
        bigPruColId = sortedUniqPrunedColours[-1]
        bigPruCol = getWeightOfColour(bigPruColId, sortedPrunedEdges)
        secBigPruColId = sortedUniqPrunedColours[-2]
        secBigPruCol = getWeightOfColour(secBigPruColId, sortedPrunedEdges)
    elif len(sortedUniqPrunedColours) == 1:
        minPruColId = sortedUniqPrunedColours[0]
        minPruCol = getWeightOfColour(minPruColId, sortedPrunedEdges)
        secMinPruColId = None
        secMinPruCol = None
        bigPruColId = sortedUniqPrunedColours[-1]
        bigPruCol = getWeightOfColour(bigPruColId, sortedPrunedEdges)
        secBigPruColId = None
        secBigPruCol = None
    else:
        minPruColId = None
        minPruCol = None
        secMinPruColId = None
        secMinPruCol = None
        bigPruColId = None
        bigPruCol = None
        secBigPruColId = None
        secBigPruCol = None

    rk = 1

    logging.info('Writing data after %s pruning %s with %s gama value',
                 algorithm, args.input_file[0], gama)
    writer.writerow({"Graph": args.input_file[0], "Algorithm": algorithm,
                     "Gama": str(gama), "Running_time": str(runningTime),
                     "rk": str(rk), "Number_of_vertices": str(numOfVer),
                     "Number_of_edges_before": str(numOfEdgBef),
                     "Number_of_edges_after": str(numOfEdgAft),
                     "Quantity_of_colours_before": str(numOfColBef),
                     "Quantity_of_colours_after": str(numOfColAft),
                     "Smallest_edge_of_initial_graph": str(smalEdgOfInitG),
                     "Second_smallest_edge_of_initial_graph":
                     str(secSmalEdgOfInitG),
                     "Smallest_edge_of_processed_graph": str(smalEdgOfProcG),
                     "Second_smallest_edge_of_processed_graph":
                     str(secSmalEdgOfProcG),
                     "Minimal_pruned_edge": str(minPruEdg),
                     "Second_minimal_pruned_edge": str(secMinPruEdg),
                     "Biggest_edge_of_initial_graph": str(bigEdgOfInitG),
                     "Second_biggest_edge_of_initial_graph":
                     str(secBigEdgOfInitG),
                     "Biggest_edge_of_processed_graph": str(bigEdgOfProcG),
                     "Second_biggest_edge_of_processed_graph":
                     str(secBigEdgOfProcG),
                     "Biggest_pruned_edge": str(bigPruEdg),
                     "Second_biggest_pruned_edge": str(secBigPruEdg),
                     "rOfSumsk": str(rOfSumsk),
                     "Minimal_pruned_colour_id": str(minPruColId),
                     "Minimal_pruned_colour": str(minPruCol),
                     "Second_minimal_pruned_colour_id": str(secMinPruColId),
                     "Second_minimal_pruned_colour": str(secMinPruCol),
                     "Biggest_pruned_colour_id": str(bigPruColId),
                     "Biggest_pruned_colour": str(bigPruCol),
                     "Second_biggest_pruned_colour_id": str(secBigPruColId),
                     "Second_biggest_pruned_colour": str(secBigPruCol),
                     "Is_homo": str(isHomo),
                     "Has_peptide": str(hasPeptide),
                     "Area_sum_before":str(arSumBef),
                     "Area_sum_after": str(arSumAft)})
    ofh.close()


def writeCentralityInfo(G):
    logging.info('Started supplementing info to %s ', args.input_file[0])
    readGraph(G, args.input_file[0])
    reverseEdges(G)
    centralityDict = nx.edge_betweenness_centrality(G, weight="weight")
    reverseEdges(G)
    nx.set_edge_attributes(G, centralityDict, "betweenness")

    pruningDict = {}
    for i in centralityDict:
        pruningDict[i] = 0

    firstColourInSort = None
    edgesForPruning = []
    edgesForPruningCentrality = []
    for v1, v2, data in sorted(G.edges(data=True),
                               key=lambda x: (x[2]["betweenness"],
                               x[2]["weight"], x[2]["colour"])):
        # If new colour is reached:
        if firstColourInSort != data["colour"]:
            # 1) Old colour's edges values are changed in pruningDict to 1 if
            #    all betweennesses are zeroes
            isAllCentralitiesZeroes = True
            for i in edgesForPruningCentrality:
                if i != 0:
                    isAllCentralitiesZeroes = False
            if isAllCentralitiesZeroes is True and \
               len(edgesForPruningCentrality) > 0:
                for i in edgesForPruning:
                    pruningDict[tuple(i)] = 1
            firstColourInSort = data["colour"]
            # 2) Data is collected if betweenness is equal to 0
            if data["betweenness"] == 0:
                edgesForPruning = [[v1, v2]]
                edgesForPruningCentrality = [data["betweenness"]]
            # 3) Algorithm is stopped because there are no more colours with
            #    all betweenness values equal to 0
            else:
                break
        # Otherwise edges data is collected until other new colour is reached.
        else:
            edgesForPruning.append([v1, v2])
            edgesForPruningCentrality.append(data["betweenness"])

    nx.set_edge_attributes(G, pruningDict, "pruning")

    ofh = open(str(args.input_file[0]) + ".txt", "w")

    ofh.write("\t".join(map(str, ["pdb_id", "biounit_no", "d1", "d1_symmetry",
                                  "d1_cluster95", "d2", "d2_symmetry",
                                  "d2_cluster95", "interaction_id",
                                  "interface_id", "i_sequence_cluster_95",
                                  "i_cluster_95_50", "area", "complex_id",
                                  "to_be_pruned",
                                  "betweenness_centrality"])) + "\n")
    with open(args.input_file[0], "r") as ifh:
        ifh.readline()
        edge = ifh.readline().split()
        while edge:
            edgeName1 = (str(edge[2]) + str(edge[3]), str(edge[5]) +
                         str(edge[6]))
            edgeName2 = (str(edge[5]) + str(edge[6]), str(edge[2]) +
                         str(edge[3]))
            edge.append(G.graph["complexID"])
            if edgeName1 in pruningDict:
                edge.append(pruningDict[edgeName1])
            else:
                edge.append(pruningDict[edgeName2])
            if edgeName1 in centralityDict:
                edge.append(centralityDict[edgeName1])
            else:
                edge.append(centralityDict[edgeName2])
            ofh.write("\t".join(map(str, edge)) + "\n")
            edge = ifh.readline().split()
    ifh.close()
    ofh.close()
    logging.info('Finished supplementing info to %s ', args.input_file[0])

if args.p is True or (args.s is False and args.d is False):
    if args.o is False:
        for gamma10 in range(1, 8):
            gamma = gamma10 * 0.1
            G = nx.Graph()
            writePruningInfo(G, "NAC", gamma)

        for gamma10 in range(1, 8):
            gamma = gamma10 * 0.1
            G = nx.Graph()
            writePruningInfo(G, "NACEC", gamma)

        for gamma10 in range(1, 8):
            gamma = gamma10 * 0.1
            G = nx.Graph()
            writePruningInfo(G, "NACEFC", gamma)

        for gamma10 in range(1, 8):
            gamma = gamma10 * 0.1
            G = nx.Graph()
            writePruningInfo(G, "NMAC", gamma)

        for gamma10 in range(1, 8):
            gamma = gamma10 * 0.1
            G = nx.Graph()
            writePruningInfo(G, "NMACEC", gamma)

        for gamma10 in range(1, 8):
            gamma = gamma10 * 0.1
            G = nx.Graph()
            writePruningInfo(G, "NMACEFC", gamma)

        for gamma10 in range(1, 8):
            gamma = gamma10 * 0.1
            G = nx.Graph()
            writePruningInfo(G, "NMAvAC", gamma)

        for gamma10 in range(1, 8):
            gamma = gamma10 * 0.1
            G = nx.Graph()
            writePruningInfo(G, "NMAvACEC", gamma)

        for gamma10 in range(1, 8):
            gamma = gamma10 * 0.1
            G = nx.Graph()
            writePruningInfo(G, "NMAvACEFC", gamma)

        for gamma10 in range(1, 8):
            gamma = gamma10 * 0.1
            G = nx.Graph()
            writePruningInfo(G, "BF", gamma)

        for gamma10 in range(5, 11):
            gamma = gamma10 * 0.1
            G = nx.Graph()
            writePruningInfo(G, "NLRk", gamma)

        for gamma10 in range(5, 10):
            gamma = gamma10 * 0.1
            G = nx.Graph()
            writePruningInfo(G, "NLROSk", gamma)

        for gamma10 in range(5, 11):
            gamma = gamma10 * 0.1
            G = nx.Graph()
            writePruningInfo(G, "NLFRk", gamma)

        for gamma10 in range(5, 10):
            gamma = gamma10 * 0.1
            G = nx.Graph()
            writePruningInfo(G, "NLFROSk", gamma)

        G = nx.Graph()
        writePruningInfo(G, "REBC500", 1)

    G = nx.Graph()
    writePruningInfo(G, "REBC", 1)

if args.s is True:
     G = nx.Graph()
     writeCentralityInfo(G)

if args.d is True:
    G = nx.Graph()
    readGraph(G, args.input_file[0])
    if not nx.is_connected(G):
        print(args.input_file[0])
